/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package beans;

/**
 *
 * @author manol
 */
public class FileBean {
    private String fileName;
    private String fileUser;
    private int fileRegNumber;
    
    public FileBean()
    {}
    public FileBean(String fileName,
                    String fileUser,
                    int fileRegNumber)
    {
        this.fileName = fileName;
        this.fileUser = fileUser;
        this.fileRegNumber = fileRegNumber;
    }
    
    public String getFileName()
    {
        return fileName;
    }
    
    public String getFileUser()
    {
        return fileUser;
    }
    
    public int getFileRegNumber()
    {
        return fileRegNumber;
    }
    
}
