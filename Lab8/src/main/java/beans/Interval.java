/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package beans;

import java.io.Serializable;
import java.time.LocalTime;
import javafx.util.Pair;
import javax.inject.Named;

/**
 *
 * @author manol
 */
@Named
public class Interval implements Serializable{
    private final Pair<LocalTime,LocalTime> mPair;
    public Interval()
    {
        mPair = null;
    }
    
    public Interval(Pair<LocalTime,LocalTime> pair)
    {
        this.mPair = pair;
    }
    
    
    public Pair<LocalTime,LocalTime> getPair(){
        return this.mPair;
    }
}
