/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package beans;

import interceptors.Logged;
import javax.inject.Named;
import javax.validation.constraints.NotNull;
import models.user.UserType;

/**
 *
 * @author manol
 */
@Logged
@Named
public class UserBean {
    private String userName;
    private String password;
    private UserType userType;
    
    
    public void  setUserName(@NotNull String userName)
    {
        this.userName = userName;
    }
    
    public String getUserName()
    {
        return userName;
    }
   
    
    public void  setPassword(@NotNull String password)
    {
        this.password = password;
    }
    
    public String getPassword()
    {
        return password;
    }
    
    public void  setUserType(@NotNull UserType userType)
    {
        this.userType = userType;
    }
    
    public UserType getUserType()
    {
        return userType;
    }
}
