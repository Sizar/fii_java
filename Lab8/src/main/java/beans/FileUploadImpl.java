/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package beans;

import java.io.IOException;
import java.io.InputStream;
import org.primefaces.model.file.UploadedFile;

/**
 *
 * @author manol
 */
public class FileUploadImpl implements UploadedFile {

    private final String fileName;
    private final byte[] content;
    private final String contentType;
    private final long size;
    
    public FileUploadImpl(String fileName,
                          byte[] content,
                          String contentType)
    {
        this.fileName = fileName;
        this.content = content;
        this.contentType = contentType;
        size = content.length;
    }
    
    @Override
    public String getFileName() {
        return fileName;
    }

    @Override
    public InputStream getInputStream() throws IOException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public byte[] getContent() {
        return content;
    }

    @Override
    public String getContentType() {
        return contentType;
    }

    @Override
    public long getSize() {
        return size;
    }

    @Override
    public void write(String string) throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
}
