/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package repositories;

import entitys.FileEntity;
import entitys.UserEntity;
import java.util.List;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.inject.Default;
import javax.inject.Named;
import javax.inject.Singleton;

/**
 *
 * @author manol
 */
@Stateless
public class FileRepository  extends DataRepository<FileEntity, Integer>{
    
    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public List<FileEntity> list()
    {
        System.out.println("repositories.AbstractDataMockRepository.persist() "+ this);
        return em.createNamedQuery("getFile").getResultList();
    }
    
    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public FileEntity findById(Integer id) {
        return id == null ? null : em.find(FileEntity.class, id);
    }
    
    
}
