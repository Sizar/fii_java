/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package repositories;

import entitys.UserAbstractEntity;
import entitys.FileEntity;
import entitys.UserEntity;
import java.util.List;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Singleton;

/**
 *
 * @author manol
 */
@Stateless
public class UserRepository extends DataRepository<UserEntity, Integer>{    
    
    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public List<UserEntity> list()
    {
        System.out.println("repositories.AbstractDataMockRepository.persist() "+ this);
        return em.createNamedQuery("getUsers").getResultList();
    }
    
    @Override
    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public boolean contains(UserEntity entity)
    {
        System.out.println("####@@@@@repositories.UserRepository.contains() ->");
        boolean retVal = false;
        for(UserEntity ent : list())
        {
            if(ent.equals(entity))
            {
                retVal = true;
                break;
            }
        }
        System.out.println("####@@@@@repositories.UserRepository.contains() -> + " + retVal);
        return retVal;
    }
    
    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public boolean containsName(UserEntity entity)
    {
        boolean retVal = false;
        for(UserEntity ent : list())
        {
            if(ent.getName().equals(entity.getName()))
            {
                retVal = true;
                break;
            }
        }
        return retVal;
    }
    
    
    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public UserEntity findById(Integer id) {
        return id == null ? null : em.find(UserEntity.class, id);
    }
    
    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public UserEntity findByName(String  name) {
        UserEntity retVal = null;
        for (UserEntity ent : list())
        {
            if (ent.getName().equals(name))
            {
                retVal = ent;
                break;
            }
        }
        System.out.println("@@@@@@@@#@#@#@#@#neme + " + name + " ->  " + retVal + " -id-> " + retVal.getId());
        return retVal;
    }
}
