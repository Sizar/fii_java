/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package repositories.mocks;


import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.Lock;
import javax.ejb.LockType;
import javax.inject.Singleton;

/**
 *
 * @author manol
 * @param <T>
 */

@Singleton
public abstract class AbstractDataMockRepository<T> implements Serializable {
    List<T> list;
    
    public AbstractDataMockRepository()
    {
        list = new ArrayList<>();
    }
    
    @Lock(LockType.WRITE)
    public void persist(T entity) 
    {
        System.out.println("repositories.AbstractDataMockRepository.persist() "+ this + entity);
        list.add(entity);
    }
    
    @Lock(LockType.WRITE)
    public void remove(T entity) 
    {
        list.remove(entity);
    }
    
    @Lock
    public boolean contains(T entity)
    {
        return list.contains(entity);
    }
    
    @Lock
    public List<T> list()
    {
        System.out.println("repositories.AbstractDataMockRepository.persist() "+ this);
        return list;
    }
}
