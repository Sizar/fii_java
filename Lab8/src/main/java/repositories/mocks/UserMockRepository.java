/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package repositories.mocks;

import javax.enterprise.context.ApplicationScoped;
import models.user.User;

/**
 *
 * @author manol
 */
@ApplicationScoped
public class UserMockRepository extends AbstractDataMockRepository<User> {}
