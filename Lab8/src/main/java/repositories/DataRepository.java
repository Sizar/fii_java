/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package repositories;

import entitys.UserAbstractEntity;
import java.io.Serializable;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author manol
 * @param <T>
 * @param <ID>
 */
public abstract class DataRepository<T , ID extends Serializable> 
{
    @PersistenceContext 
    protected EntityManager em;
    
    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public void persist(T entity) {
        em.persist(entity);
    }
    
    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public void update(T entity) {
        em.merge(entity);
    }

    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public void delete(T entity) {
        em.remove(em.contains(entity) ? entity : em.merge(entity));
    }
    
    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public boolean contains(T entity)
    {
        return em.contains(entity);
    }
}
