/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package services;

import beans.FileUploadImpl;
import entitys.FileEntity;
import entitys.UserEntity;
import interceptors.Logged;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.EJB;
import javax.enterprise.inject.Default;
import javax.inject.Inject;
import models.files.File;
import models.user.Admin;
import models.user.Guest;
import models.user.User;
import models.user.UserType;
import org.primefaces.component.fileupload.FileUpload;
import org.primefaces.model.file.UploadedFile;
import org.primefaces.model.*;
import org.primefaces.model.file.CommonsUploadedFile;
import repositories.FileRepository;
import repositories.UserRepository;
import repositories.mocks.FileMockRepository;
import repositories.mocks.UserMockRepository;

/**
 *
 * @author manol
 */
@Logged 
@Default
public class UserInteractionService implements UserInteractionInterface
{
    /*@Inject 
    private UserMockRepository userRepository;
    */
    //@Inject 
    //private FileMockRepository fileRepository;
    
    @EJB
    private FileRepository fileReop;
    
    @EJB
    private UserRepository userRepo;
    
    @Override
    public boolean login(final User user) {
        System.out.println("services.UserInteractionService.login()!");
        return userRepo.contains(toUserEntity(user));
    }

    @Override
    public boolean register(final User user) {
        boolean retVal = false;
        UserEntity entity = toUserEntity(user);
        if (!userRepo.containsName(entity))
        {
            userRepo.persist(entity);
            retVal = true;
        }
        return retVal;
    }

    @Override 
    public List<File> listOfFiles() {
        List<File> retList = new ArrayList<>();
        for(FileEntity fEntity :  fileReop.list())
        {
            retList.add(toFile(fEntity));
        }
        return retList;
    }

    @Override
    public void upload(final File file) {
        
        fileReop.persist(tofileEntity(file));
    }
    
    private UserEntity toUserEntity(User user) {
        UserEntity ent = new UserEntity();
        ent.setName( user.getUserName());
        ent.setPassword(user.getPassword());
        ent.setType(user.getUserType());
        return ent;
    }
    
    private FileEntity tofileEntity(File file)
    {
        FileEntity fEnt = new FileEntity();
        fEnt.setBytes(file.getUploadedFile().getContent());
        fEnt.setName(file.getUploadedFile().getFileName());
        fEnt.setType(file.getUploadedFile().getContentType());
        fEnt.setRegNumber(file.getRegistrationNumber());
        fEnt.setUserEntity(userRepo.findByName(file.getUser().getUserName()));
        return fEnt;
    }
    
    private User toUser(UserEntity userEntity) {
        User user = userEntity.getType() == UserType.Admin ?
                new Admin(userEntity.getName(), userEntity.getPassword()):
                new Guest(userEntity.getName(), userEntity.getPassword());
         
        return user;
    }
    
     private File toFile(FileEntity fileEntity) {
        File retVal;
        User user = toUser(fileEntity.getUserEntity());
        UploadedFile uf = new FileUploadImpl(fileEntity.getName(), fileEntity.getBytes(), fileEntity.getType());
        
        retVal = new File(user,uf);//(@NotNull User user, @NotNull UploadedFile uploadedFile)
        return retVal;
    }
}
