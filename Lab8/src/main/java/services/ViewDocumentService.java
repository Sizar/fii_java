/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package services;

import java.util.ArrayList;
import java.util.List;
import javax.inject.Inject;
import javax.jws.HandlerChain;
import javax.jws.WebService;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import models.files.File;
import models.user.User;

/**
 *
 * @author manol
 */
@WebService(serviceName = "ViewDocumentService")
@HandlerChain(file = "ViewDocumentService_handler.xml")
public class ViewDocumentService {    
    @Inject UserInteractionInterface userInteraction;
    /**
     * This is a sample web service operation
     * @param userName
     * @return 
     */
    @WebMethod(operationName = "getFiles")
    public List<String> getFiles(@WebParam(name = "userName") String userName) 
    {
        List<String>  retVal = new ArrayList<>(); 
        List<File> auxContainer;
        
        if (userName == null)
        {
            auxContainer = userInteraction.listOfFiles();
        }
        else
        {
            auxContainer = new ArrayList<>(); 
            System.out.println("services.ViewDocumentService.getFiles() ");
            userInteraction.listOfFiles().forEach(f -> {
                User user = f.getUser();
                if(f.getUser().getUserName() == null ? userName == null : f.getUser().getUserName().equals(userName))
                {
                    auxContainer.add(f);
                }
            });
        }
        
        auxContainer.forEach(f -> {
                StringBuilder sb = new StringBuilder();
                //listToAdd.add(regNumber);
                sb.append(f.getRegistrationNumber());
                sb.append("\\");
                sb.append(f.getUploadedFile().getFileName());
                sb.append("\\");
                sb.append(f.getUploadedFile().getContentType());
                sb.append("\\");
                sb.append(f.getUploadedFile().getContent());
                retVal.add(sb.toString());
            });
        
        return retVal;
    }
}
