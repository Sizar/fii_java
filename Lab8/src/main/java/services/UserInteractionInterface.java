/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package services;

import java.util.List;
import javax.enterprise.event.Observes;
import models.files.File;
import models.user.User;

/**
 *
 * @author manol
 */
public interface UserInteractionInterface {
    public abstract boolean login(User user);
    public abstract boolean register(User user);
    public abstract void upload(File file);
    public abstract List<File> listOfFiles();
}
