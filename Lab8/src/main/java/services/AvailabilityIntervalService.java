/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package services;

import beans.Interval;
import configuration.Environment;
import java.io.Serializable;
import java.time.LocalTime;
import javafx.util.Pair;
import javax.annotation.PostConstruct;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
/**
 *
 * @author manol
 */
@ApplicationScoped
public class AvailabilityIntervalService implements Serializable {
    @Inject Environment mEnvironmen;
    
    private Interval mDevPair;
    private Interval mReleaseDateInterval;
    
    @PostConstruct
    public void init()
    {
        mDevPair = new Interval(new Pair( LocalTime.now(), LocalTime.now().plusMinutes(50)));
        mReleaseDateInterval = new Interval(new Pair(LocalTime.of(10,00,0), LocalTime.of(11,00,00)));
    }
    private Interval getCurrentInterval()
    {
        Interval interval;
        if (mEnvironmen.getEnvType().isDevelopement())
            interval = mDevPair;
        else
            interval = mReleaseDateInterval;
        return interval;
    }
    
    public boolean isAvailable()
    {
        /*
        System.out.println("left: " + LocalTime.now().isAfter(getCurrentInterval().getPair().getKey()));
        System.out.println("right: " + LocalTime.now().isBefore(getCurrentInterval().getPair().getValue()));
        System.out.println("right left: " + LocalTime.now().getHour());
        System.out.println("right right: " + getCurrentInterval().getPair().getValue().getHour());
        */
        return LocalTime.now().isAfter(getCurrentInterval().getPair().getKey()) &&
                LocalTime.now().isBefore(getCurrentInterval().getPair().getValue());
    }
    
    public LocalTime getLeftBoundOfInterval()
    {
        return getCurrentInterval().getPair().getKey();
    }
    
    public LocalTime getRightBoundOfInterval()
    {
        return getCurrentInterval().getPair().getValue();
    }
}
