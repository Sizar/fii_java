/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cache;

import beans.FileBean;
import services.UserInteractionInterface;
import java.util.ArrayList;
import java.util.List;
import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.event.Observes;
import javax.inject.Inject;
import javax.inject.Named;
import models.files.File;

/**
 *
 * @author manol
 */

@Named
@ApplicationScoped
public class FilesCache {
    List<File> listCache;
    @Inject UserInteractionInterface userInteraction;
    
    public void uploadFile(@Observes File file)
    {    
        System.out.println("cache.FilesCache.uploadFile()");
        listCache = null;
    }
    
    public List<FileBean> getListCache() 
    {
        if (listCache == null)
        {
            listCache = userInteraction.listOfFiles();
        }
        List<FileBean> retList = new ArrayList<>();
        for(File f : listCache)
        {
            System.out.println("views.AdminView.setFile(): -->" + f.getUploadedFile().getFileName() );
            retList.add(new FileBean(f.getUploadedFile().getFileName(),
                        f.getUser().getUserName(),
                        f.getRegistrationNumber())
            );
        }
        return retList;
    }
}
