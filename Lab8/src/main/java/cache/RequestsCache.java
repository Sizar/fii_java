/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cache;

import java.util.HashMap;
import java.util.Map;
import javax.ejb.Lock;
import javax.ejb.Stateless;
import javax.enterprise.event.Observes;
import javax.inject.Inject;
import javax.inject.Singleton;
import models.files.File;
import services.UserInteractionInterface;
import static javax.ejb.LockType.READ;
import static javax.ejb.LockType.WRITE;
import javax.enterprise.context.ApplicationScoped;
import javax.xml.soap.SOAPMessage;

/**
 *
 * @author manol
 */
@Singleton
public class RequestsCache {
    RequestsCache()
    {
        cache = new HashMap<>();
    }
    
    private Map<String, SOAPMessage> cache;
    private String nextValue;
    
    public void resetMap(@Observes File file)
    {    
        System.out.println("cache.RequestsCache.resetMap()");
        cache.clear();
    }
    
    @Lock
    public boolean contains(String key)
    {
        return cache.containsKey(key);
    }
    
    @Lock
    public void toBeStored(String key)
    {
        nextValue = key;
    }
    
    @Lock
    public SOAPMessage get(String key)
    {
        return cache.get(key);
    }
    
    @Lock(WRITE)
    public void add(SOAPMessage value)
    {
        cache.put(nextValue, value);
    }
}
