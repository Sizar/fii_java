/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package producers;

import java.util.Random;
import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.inject.Default;
import javax.enterprise.inject.Produces;

/**
 *
 * @author manol
 */
@ApplicationScoped
@Default
public class NumberGenerator {
    
    private static final int MAX = 100;
    private static final Integer[] hashMap = new Integer[MAX];
    
    @Produces @RegistrationNumber int getRegistrationNumber() 
    {
        System.out.println("hHHhehrehrerhae asd asd producers.NumberGenerator.getRegistrationNumber()");
        Random rd = new Random();
        int number;
        do{
            number = rd.nextInt(100);
        }while (hashMap[number] == 0);
        
        hashMap[number] = 1;
        return number;
    }
}
