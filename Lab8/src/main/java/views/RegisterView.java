/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package views;

import beans.UserBean;
import services.UserInteractionInterface;
import java.io.IOException;
import javax.enterprise.context.RequestScoped;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import models.user.Admin;
import models.user.Guest;
import models.user.User;
import models.user.UserType;

/**
 *
 * @author manol
 */
@Named
@RequestScoped
public class RegisterView {
    @Inject UserBean userBean;
    
    @Inject UserInteractionInterface userInteraction;

    
    public UserBean getUserBean()
    {
        return this.userBean;
    }
    
   
    
    public UserType[] getUserTypes()
    {
        return UserType.values();
    }
    
    public void register() throws IOException
    {
        User user = null;
        switch(userBean.getUserType())
        {
            case Admin:
            {
                user = new Admin(userBean.getUserName(),userBean.getPassword());
                break;
            }
            case Guest:
            {
                user = new Guest(userBean.getUserName(),userBean.getPassword());
                break;
            }
            default:
            {
                System.err.println("Error, unknown UserType received: {}" + userBean.getUserType());
            }
        }
        if(userInteraction.register(user))
        {
            FacesContext.getCurrentInstance().getExternalContext().redirect("faces/Login.xhtml");
        }
    }
}
