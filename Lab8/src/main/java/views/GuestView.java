/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package views;

import javax.enterprise.context.RequestScoped;
import javax.enterprise.event.Event;
import javax.enterprise.inject.Any;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.validation.constraints.NotNull;
import models.files.File;
import models.user.User;
import org.primefaces.model.file.UploadedFile;
import services.UserInteractionInterface;

/**
 *
 * @author manol
 */
@Named
@RequestScoped
public class GuestView {
    private UploadedFile file;
    
    @Inject UserInteractionInterface userInteraction;
    @Inject @Any Event<File> fileUploadEvent;
    public UploadedFile getFile() {
        return file;
    }
    
    public void setFile(@NotNull UploadedFile file) {
        FacesContext context = FacesContext.getCurrentInstance();

        HttpServletRequest request = (HttpServletRequest) context
                    .getExternalContext().getRequest();
        HttpSession appsession = request.getSession(true);
            
        User user = (User)appsession.getAttribute("user");
        File uploadedFile = new File(user, file);
        userInteraction.upload(uploadedFile);
        fileUploadEvent.fire(uploadedFile);
    }
    
    public void refresh() 
    {
        //nothing to be done 
    }
}
