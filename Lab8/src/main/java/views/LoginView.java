/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package views;

import javax.enterprise.context.RequestScoped;
import javax.inject.Named;
import beans.UserBean;
import java.io.IOException;
import javax.annotation.PostConstruct;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import models.user.Admin;
import models.user.Guest;
import models.user.User;
import services.UserInteractionInterface;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import services.UserInteractionService;
/**
 *
 * @author manol
 */
@Named
@RequestScoped
public class LoginView {
    @Inject UserBean userBean;
    @Inject UserInteractionInterface userInteraction;

    
    public UserBean getUserBean()
    {
        return this.userBean;
    }
    
    public void login() throws IOException
    {
        User admin = new Admin(userBean.getUserName(), userBean.getPassword());
        User guest = new Guest(userBean.getUserName(), userBean.getPassword());
        if (userInteraction.login(admin) || userInteraction.login(guest))
        {
            FacesContext context = FacesContext.getCurrentInstance();

            HttpServletRequest request = (HttpServletRequest) context
                    .getExternalContext().getRequest();
            HttpSession appsession = request.getSession(true);
            
            String url = (String)appsession.getAttribute("redirectUrl");
            
            System.out.println("->views.LoginView.login()" + url);
            FacesContext.getCurrentInstance().getExternalContext().redirect(url);
        }
    }
}
