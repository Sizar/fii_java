/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package views;

import interceptors.Logged;
import java.time.LocalTime;
import javax.annotation.PostConstruct;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.inject.Named;
import services.AvailabilityIntervalService;

/**
 *
 * @author manol
 */
//@Logged 
@Named("indexView")
@RequestScoped
public class IndexView {
    private boolean available;
    private LocalTime leftIntervalBound;
    private LocalTime rightIntervalBound;
    
    @Inject AvailabilityIntervalService availabilityIntervalService;
    
    @PostConstruct
    public void init()
    {
        leftIntervalBound= availabilityIntervalService.getLeftBoundOfInterval();
        rightIntervalBound= availabilityIntervalService.getRightBoundOfInterval();
    }
    
    public boolean getAvailable()
    {
        available = availabilityIntervalService.isAvailable();
        return available;
    }
    
    public LocalTime getLeftIntervalBound()
    {
        return leftIntervalBound;
    }
    
    public LocalTime getRightIntervalBound()
    {
        return rightIntervalBound;
    }
}
