/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package handler;

import cache.RequestsCache;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.util.Pair;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.xml.namespace.QName;
import javax.xml.soap.SOAPElement;
import javax.xml.soap.SOAPException;
import javax.xml.soap.SOAPMessage;
import javax.xml.ws.handler.MessageContext;
import javax.xml.ws.handler.soap.SOAPHandler;
import javax.xml.ws.handler.soap.SOAPMessageContext;

/**
 *
 * @author manol
 */
public class MyMessageHandler implements SOAPHandler<SOAPMessageContext> { 
    
    @Inject RequestsCache internalChache;
    
    public boolean handleMessage(SOAPMessageContext messageContext) {
        Boolean outboundProperty = (Boolean) messageContext.get(MessageContext.MESSAGE_OUTBOUND_PROPERTY);
        boolean retVal = true;
        SOAPMessage msg = messageContext.getMessage();
        if(outboundProperty)
            System.out.println("\nOutbound m:");
        else
            System.out.println("\nInbound message:");
            
        String name, value;
        name = value = null;
        try {
            Iterator<SOAPElement> it = msg.getSOAPBody().getChildElements();
            while (it.hasNext()) {
                    SOAPElement elem = it.next();
                   // addNamespacePrefix(elem);
                   // System.out.println("\n#### -> "+ elem.getAttributes().toString());


                    Iterator itChildChildren = elem.getChildElements();
                    while (itChildChildren.hasNext()) {
                        Object obj = itChildChildren.next();
                        if ((obj instanceof SOAPElement)) {
                            SOAPElement soapElem = (SOAPElement) obj;
                            name = soapElem.getElementName().getLocalName();
                            value = soapElem.getValue();

                            QName qName = new QName(name);
                            //((SOAPElement) obj).setElementQName(qName);
                            System.out.println("\n#### -> "+ soapElem.getPrefix());
                            System.out.println("\n#### -> "+ name);
                            System.out.println("\n#### -> "+ value);
                        }
                    }
            }
            if(!internalChache.contains(name) && !outboundProperty)
            {
                System.out.println("\n not contains:");
                internalChache.toBeStored(name);
            }
            else if (internalChache.contains(name) && !outboundProperty)
            {
                System.out.println("\n contains:"); 
                messageContext.setMessage(internalChache.get(name));
                retVal = false;
            }
            else if (outboundProperty)
            {
                System.out.println("\n storing:"); 
               internalChache.add(msg);
            }
            
        } catch (SOAPException ex) {
            Logger.getLogger(MyMessageHandler.class.getName()).log(Level.SEVERE, null, ex);
        }
        return retVal;
    }
    
    @Override
    public Set<QName> getHeaders() {
        System.out.println("\ngetHeaders:");
        return Collections.EMPTY_SET; 
    } 
    
    @Override
    public boolean handleFault(SOAPMessageContext messageContext) {
        System.err.println("\nhandleFault:");
        return true;
    } 
    
    @Override
    public void close(MessageContext context) {
        System.out.println("\nclose:");
    }
}
