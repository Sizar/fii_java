/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package interceptors;

import java.io.Serializable;
import javax.interceptor.AroundInvoke;
import javax.interceptor.Interceptor;
import javax.interceptor.InvocationContext;

/**
 *
 * @author manol
 */

@Logged
@Interceptor
public class LoggedInterceptor implements Serializable 
{
    @AroundInvoke public Object logMethodEntry(InvocationContext invocationContext)throws Exception 
    {
        System.out.println("Entering method: "+ invocationContext.getMethod().getName() + 
                " in class "+ invocationContext.getMethod().getDeclaringClass().getName() + 
                " class: " + invocationContext.getMethod().getDeclaringClass().toString());
        return invocationContext.proceed();
    }
}
