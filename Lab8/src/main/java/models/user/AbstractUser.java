/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package models.user;

import java.util.Objects;
import models.user.User;
import models.user.UserType;

/**
 *
 * @author manol
 */
public abstract class AbstractUser implements User{

    private final String mUserName;
    private final String mPassword;
    private final UserType mUserType;
    
    public AbstractUser(String userName,
            String password,
            UserType userType)
    {
        mUserName = userName;
        mPassword = password;
        mUserType = userType;
    }
    
    @Override
    public String getUserName() {
        return mUserName;
    }

    @Override
    public String getPassword() {
        return mPassword;
    }

    @Override
    public UserType getUserType() {
        return mUserType;
    }
    
   @Override
    public boolean equals(final Object other){
        if(this == other){
            return true;
        }
        if(!(other instanceof User)){
            return false;
        }
        final User otherUser = (User) other;
        return (otherUser.getPassword() == null ? this.mPassword == null : otherUser.getPassword().equals(this.mPassword)) &&
                (otherUser.getUserName() == null ? this.mUserName == null : otherUser.getUserName().equals(this.mUserName)) &&
                otherUser.getUserType() == this.mUserType;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 23 * hash + Objects.hashCode(this.mUserName);
        hash = 23 * hash + Objects.hashCode(this.mPassword);
        hash = 23 * hash + Objects.hashCode(this.mUserType);
        return hash;
    }

}
