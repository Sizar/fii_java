/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package models.user;

import models.user.UserType;

/**
 *
 * @author manol
 */
public class Admin extends AbstractUser{
    public Admin(String userName, String password) {
        super(userName, password, UserType.Admin);
    }
}
