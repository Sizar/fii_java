/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package models.user;

import com.sun.org.apache.xpath.internal.operations.Equals;
import models.user.UserType;

/**
 *
 * @author manol
 */
public interface User {
    public abstract  String getUserName();
    public abstract  String getPassword();
    public abstract  UserType getUserType();
}
