/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package models.user;

import views.GuestView;

/**
 *
 * @author manol
 */
public enum UserType {
    
    Guest(){
        @Override
        public boolean isGuest() {
            return true;
        }
        
        @Override
        public boolean isAdmin() {
            return false;
        }
        
        @Override
        public String toString() {
            return "Guest";
        }
    },
    Admin(){
        @Override
        public boolean isGuest() {
            return false;
        }

        @Override
        public boolean isAdmin() {
            return true;
        }

        @Override
        public String toString() {
            return "Admin";
        }
    };
    
    public abstract boolean isGuest();
    public abstract boolean isAdmin();
    
    @Override
    public abstract String toString();
}
