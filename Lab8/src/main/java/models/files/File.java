/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package models.files;

import java.io.Serializable;
import java.util.Objects;
import javax.inject.Inject;
import javax.validation.constraints.NotNull;
import models.user.User;
import org.primefaces.model.file.UploadedFile;
import producers.RegistrationNumber;

/**
 *
 * @author manol
 */
public class File{
    private  User mUser;
    private  UploadedFile mUploadedFile;
    @Inject @RegistrationNumber int mRegistrationNumber;
    
    public File(@NotNull User user, @NotNull UploadedFile uploadedFile)
    {
        this.mUser = user;
        this.mUploadedFile =  uploadedFile;
        // System.out.println("    $$$$$$$$$$$$$$$$$$ ->" + uploadedFile.getFileName());
    }
    
    public User getUser()
    {
        return mUser;
    }
    
    public UploadedFile getUploadedFile()
    {
        return mUploadedFile;
    }
    
    public int getRegistrationNumber()
    {
        return mRegistrationNumber;
    }
    
    public void setUser(User user)
    {
        mUser = user;
    }
    
    public void setUploadedFile(UploadedFile uploadedFile)
    {
        mUploadedFile = uploadedFile;
    }
    
    public void setRegistrationNumber(int registrationNumber)
    {
        mRegistrationNumber = registrationNumber;
    }
    
    @Override
    public boolean equals(final Object other){
        if(this == other){
            return true;
        }
        if(!(other instanceof File)){
            return false;
        }
        final File otherFile = (File) other;
        return otherFile.getUser() == this.mUser &&
                otherFile.getUploadedFile() == this.mUploadedFile;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 71 * hash + Objects.hashCode(this.mUser);
        hash = 71 * hash + Objects.hashCode(this.mUploadedFile);
        return hash;
    }
}
