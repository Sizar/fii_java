/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package configuration;

import javax.enterprise.context.ApplicationScoped;

/**
 *
 * @author manol
 */

@ApplicationScoped
public class Environment {
    private final EnvType mEnvType;
    
    public Environment()
    {
        mEnvType = EnvType.Developement;
    }
    
    public EnvType getEnvType()
    {
        return mEnvType;
    }
    
    public enum EnvType
    {
        Developement(){
            @Override
            public boolean isDevelopement() {
                return true;
            }

            @Override
            public boolean isRelease() {
                return false;
            }
        },
        Release(){
            @Override
            public boolean isDevelopement() {
                return false;
            }

            @Override
            public boolean isRelease() {
                return true;
            }
        };
        
        public abstract boolean isDevelopement();
        public abstract boolean isRelease();
    }
    
    
}
