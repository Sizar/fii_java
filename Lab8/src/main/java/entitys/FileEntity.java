/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entitys;

import java.io.Serializable;
import javax.persistence.AttributeOverride;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MapsId;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import repositories.UserRepository;

/**
 *
 * @author manol
 */
@Entity
@NamedQuery(name="getFile", query="SELECT f FROM FileEntity f")
@Table(name = "FILES")
@SequenceGenerator(name = FileAbstractEntity.GENERATOR,
        sequenceName="file_id_seq_sec",
        allocationSize=50)
@AttributeOverride(name="id", column=@Column(name = "ID"))
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class FileEntity extends FileAbstractEntity<Integer> implements Serializable {
    
    @Column(name = "NAME")
    @XmlElement
    private String name;
    
    @Column(name = "TYPE")
    @XmlElement
    private String type;
    
    @Column(name = "BYTES")
    @XmlElement
    private byte[] bytes;
    
    @ManyToOne(fetch = FetchType.LAZY)
    @XmlElement
    private UserEntity user;
    
    @Column(name = "REG_NUMBER")
    @XmlElement
    private int regNumber;
    
    @Override
    public int hashCode() {
        if (getId() != null) {
            return getId().hashCode();
        }
        return super.hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        FileEntity other = (FileEntity) obj;
        if (getId() == null || other.getId() == null) {
            return false;
        }
        return getId().equals(other.getId());
    }
    
    @Override
    public String toString() {
        return "repositories.FileEntity[ id=" + getId() + " ]";
    }
    
    public void setName(@NotNull String name)
    {
        this.name = name;
    }
    
    public String getName()
    {
        return name;
    }
    
    public void setType(@NotNull String type)
    {
        this.type = type;
    }
    
    public String getType()
    {
        return type;
    }
    
    public void setBytes(@NotNull byte[] bytes)
    {
        this.bytes = bytes;
    }
    public byte[] getBytes()
    {
        return this.bytes;
    }
    
    public UserEntity getUserEntity()
    {
        return user;
    }
    public void setUserEntity(UserEntity userEntity)
    {
        this.user = userEntity;
    }
    
    public void setRegNumber(int regNumber)
    {
        this.regNumber = regNumber;
    }
    
    public int getRegNumber()
    {
        return this.regNumber;
    }
}
