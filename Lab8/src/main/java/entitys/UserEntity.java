/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entitys;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.AttributeOverride;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import models.user.User;
import models.user.UserType;
import org.eclipse.persistence.oxm.annotations.XmlInverseReference;

/**
 *
 * @author manol
 */
@Entity
@NamedQuery(name="getUsers", query="SELECT u FROM UserEntity u")
@Table(name = "USERS")
@SequenceGenerator(name = UserAbstractEntity.GENERATOR, 
        sequenceName="users_id_seq_sec",
        allocationSize=50)
@AttributeOverride(name="id", column=@Column(name = "ID"))
@XmlRootElement
public class UserEntity extends UserAbstractEntity<Integer> implements Serializable {
    @Column(name = "NAME")
    @XmlElement
    private String name;
   
    @Column(name = "PASSWORD")
    @XmlElement
    private String password;

    @Column(name = "TYPE")
    @XmlElement
    private int type;
    
    @OneToMany(
        cascade = CascadeType.ALL,
        orphanRemoval = true
    )
    @XmlElement
    private List<FileEntity> files = new ArrayList<>();
    
    public void setName(@NotNull String name)
    {
        this.name = name;
    }
    
    public String getName()
    {
        return name;
    }
    
    public void setPassword(@NotNull String password)
    {
        this.password = password;
    }
    
    public String getPassword()
    {
        return password;
    }
    
    public void setType(UserType type)
    {
        this.type = type == UserType.Admin ? 1 : 0;
    }
    
    public UserType getType()
    {
        return type == 1 ? UserType.Admin : UserType.Guest;
    }
    
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        UserEntity other = (UserEntity) obj;
        return (name == null ? other.getName() == null : name.equals(other.getName())) && 
                (password == null ? other.getPassword() == null : password.equals(other.getPassword())) && 
                getType() == other.getType();
    }
    
}
