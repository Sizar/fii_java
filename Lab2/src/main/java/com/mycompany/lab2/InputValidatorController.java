/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.lab2;

import java.io.IOException;
import java.util.Iterator;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import resources.Dictionary;
import resources.Language;
import resources.Word;

/**
 *
 * @author manol
 */
@WebServlet(name = "InputValidatorController", urlPatterns = {"/InputValidatorController"})
public class InputValidatorController extends HttpServlet {

    private String WORD_NAME = "WORD_NAME";
    private String DEFINITION_NAME = "DEFINITION_NAME";
    private String LANGUAGE_NAME = "LANGUAGE_NAME";
    private String COOKIE_NAME = "Input.language";

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
                // Set the language in a cookie 
        boolean cokieSet = false;
        Cookie theCookie = new Cookie(COOKIE_NAME, Language.RO.toString());
        Cookie cookies[] = request.getCookies();
        if (cookies != null) {
            for (int i=0; i<cookies.length; i++) {
                theCookie = cookies[i];
                if (theCookie.getName().equals(COOKIE_NAME)) 
                {
                    cokieSet = true;
                    break;
                }
            }
        }
        
        StringBuilder sb = new StringBuilder();
        sb.append("<select id=\"languageID\" name=\"").append(LANGUAGE_NAME).append("\">");
        if (cokieSet)
        {
            sb.append("<option value= ")
                    .append(theCookie.getValue())
                    .append(">")
                    .append(theCookie.getValue())
                    .append("</option>");
            
            for(Language l : Language.values())
            {
                if (!theCookie.getValue().equals(l.toString()))
                {
                    sb.append("<option value= ")
                        .append(l.toString())
                        .append(">")
                        .append(l.toString())
                        .append("</option>");
                }
                else{ }
            }
        }
        else
        {
            for(Language l : Language.values())
            {
                    sb.append("<option value= ")
                        .append(l.toString())
                        .append(">")
                        .append(l.toString())
                        .append("</option>");
                
            }
        }
        sb.append("</select>");
        request.setAttribute("Languages", sb.toString());
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        HttpSession session = request.getSession(true);
        printReqInfo(request);
        
        getServletConfig().getServletContext().getRequestDispatcher("/MyCAPTCHA").include(request, response);
        
        String word = request.getParameter(WORD_NAME);
        String definition = request.getParameter(DEFINITION_NAME);
        String languageAsString = request.getParameter(LANGUAGE_NAME);
        System.out.println("word:" + word +", definition: "+ definition + ", language: "+ languageAsString);
        //Integer languageAsInteger = Language.fromString(languageAsString);
        Language language = Language.fromString(languageAsString);

        Cookie theCookie = new Cookie(COOKIE_NAME, language.toString());
        theCookie.setComment("The language selected");
        theCookie.setMaxAge(30*60); //in seconds
        response.addCookie(theCookie);
        
        
        Dictionary dictionaryBean = (Dictionary) getServletContext().getAttribute(Dictionary.PROP_WORDSLIST_PROPERTY);
        if (dictionaryBean == null)
        {
            dictionaryBean = new Dictionary();
        }
        if (!word.isEmpty() && !definition.isEmpty())
        {
            Word bean = new Word();
            bean.setLanguageProperty(language);
            bean.setDefinitionProperty(definition);
            bean.setWordProperty(word);
            if(!dictionaryBean.addWord(bean))
            {
                    RequestDispatcher dispatcher = getServletConfig().getServletContext().getRequestDispatcher("/error.jsp");
                    Integer errorCode = 400;
                    String s = "word already exists!";
                    IOException e = new IOException("word already exists!");
                    request.setAttribute("javax.servlet.error.status_code", errorCode);
                    request.setAttribute( "javax.servlet.error.message", s);
                    request.setAttribute("javax.servlet.error.exception", e);
                    dispatcher.forward(request, response);
             
            }
            getServletContext().setAttribute(Dictionary.PROP_WORDSLIST_PROPERTY,dictionaryBean);
            RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/results.jsp");
            dispatcher.forward(request, response);
        }
        else
        {
                RequestDispatcher dispatcher = getServletConfig().getServletContext().getRequestDispatcher("/error.jsp");
                Integer errorCode = 400;
                String s = "Empty fileds found!";
                IOException e = new IOException("Empty fileds found!");
                request.setAttribute("javax.servlet.error.status_code", errorCode);
                request.setAttribute( "javax.servlet.error.message", s);
                request.setAttribute("javax.servlet.error.exception", e);
                dispatcher.forward(request, response);
           
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

    public void printReqInfo(HttpServletRequest request)
    {
        StringBuilder sbParamValues = new StringBuilder();
        Iterator<String[]> iterator = request.getParameterMap().values().iterator();
        // while loop
        while (iterator.hasNext())
        {
            sbParamValues.append(iterator.next()[0]);
        }
                System.out.print("# METHOD USEDL:" + request.getMethod() + 
                ", Addr:" + request.getRemoteAddr() + 
                ", user-agent: " + request.getHeader("User-Agent") + 
                ", language(s): " + request.getHeader("Accept-Language") +
                ", parameters names: " + request.getParameterMap().keySet().toString() +
                ",parameters values: " + sbParamValues.toString());
    }
    
}
