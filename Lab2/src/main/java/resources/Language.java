/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package resources;

import java.io.IOException;
import java.util.Objects;

/**
 *
 * @author manol
 */
public enum Language{
    RO(0){
        @Override
        public String toString() {
            return "RO"; //To change body of generated methods, choose Tools | Templates.
        }
    },
    ENG(1){
        @Override
        public String toString() {
            return "ENG"; //To change body of generated methods, choose Tools | Templates.
        }
    },
    FR(2){
        @Override
        public String toString() {
            return "FR"; //To change body of generated methods, choose Tools | Templates.
        }
    },
    IT(3){
        @Override
        public String toString() {
            return "IT"; //To change body of generated methods, choose Tools | Templates.
        }
    };
    
    private Integer value;
    Language(Integer value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return Integer.toString(value);
    }
    public void setValue(Integer value) throws IOException
    {
        if (value > IT.value || value < RO.value)
        {
            throw new IOException("The language that was provided is not supported!");
        }
        this.value = value;
    }
    public Integer getValue()
    {
        return value;
    }
    
    public boolean equals(Language other)
    {
        return Objects.equals(value, other.getValue());
    }
    
    public static Language fromString(String s)
    {
        for(Language l : Language.values())
        {
            if(s.equals(l.toString()))
                return l;
        }
        return null;
    }
    
}
