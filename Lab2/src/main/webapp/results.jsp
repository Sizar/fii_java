<%-- 
    Document   : results
    Created on : Oct 21, 2020, 5:12:53 PM
    Author     : manol
--%>

<%@page import="resources.Dictionary"%>
<%@page import="resources.Word"%>
<%@page contentType="text/html" pageEncoding="UTF-8" errorPage="error.jsp"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP RESLT Page</title>
        <style>
        table {
            font-family: arial, sans-serif;
            border-collapse: collapse;
            width: 100%;
        }

        td, th {
            border: 1px solid #dddddd;
            text-align: left;
            padding: 8px;
        }

        tr:nth-child(even) {
            background-color: #dddddd;
        }
</style>
    </head>
    <body>
        <%
        Dictionary dictionary = (Dictionary) getServletContext().getAttribute(Dictionary.PROP_WORDSLIST_PROPERTY);

        if ( dictionary == null)
        {
            throw new Exception("The dictionary was empty at result");
        } 
        %>
         <table id="sampTable">
             <tr>
                <th>WORD</th>
                <th>DEFINITION</th>
                <th>LANGUAGE</th>
            </tr>
        <% for(Word w : dictionary.getWordsListProperty())
        {
                out.print("<tr>");    
                out.print("<th> " + w.getWordProperty()+ "</th>" 
                        + "<th> " + w.getDefinitionProperty()+"</th>" 
                        + "<th> " + w.getLanguageProperty().toString()+"</th>");  
                out.print("</tr>");
        }
        %>  
        
        </table>
        <form action="input.jsp" method="GET">
            <input type="submit" value="Back to Input" />
        </form>
    </body>
</html>
