<%-- 
    Document   : input
    Created on : Oct 21, 2020, 3:07:13 PM
    Author     : manol
--%>

<%@page import="resources.Language"%>
<%@page contentType="text/html" pageEncoding="UTF-8" errorPage="error.jsp"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>

        <form action="/Lab2/InputValidatorController" method="POST">
            
            <jsp:include page="/MyCAPTCHA"/>
            <%
                out.print(request.getAttribute("MyCAPTCHA"));
            %>
            <label for="fname">word:      </label> 
            <input type="text" id="wordID" name="WORD_NAME" contenteditable="false">  
            <br><br>
            
            <label for="fname">definition:</label> 
            <input type="text" id="definitionID" name="DEFINITION_NAME" contenteditable="true"> 
            <br><br>
            <label for="fname">language:  </label> 
            <jsp:include page="/InputValidatorController"/>
            <%
                out.print(request.getAttribute("Languages"));
            %>

            <br><br>
            <input type="submit" value="Submit">
        </form> 
        
    </body>
</html>
