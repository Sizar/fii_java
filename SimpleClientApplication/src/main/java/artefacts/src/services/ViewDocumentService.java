
package services;

import java.util.List;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.ws.Action;
import javax.xml.ws.RequestWrapper;
import javax.xml.ws.ResponseWrapper;


/**
 * This class was generated by the JAX-WS RI.
 * JAX-WS RI 2.2.9-b130926.1035
 * Generated source version: 2.2
 * 
 */
@WebService(name = "ViewDocumentService", targetNamespace = "http://services/")
@XmlSeeAlso({
    ObjectFactory.class
})
public interface ViewDocumentService {


    /**
     * 
     * @param userName
     * @return
     *     returns java.util.List<java.lang.String>
     */
    @WebMethod
    @WebResult(targetNamespace = "")
    @RequestWrapper(localName = "getFiles", targetNamespace = "http://services/", className = "services.GetFiles")
    @ResponseWrapper(localName = "getFilesResponse", targetNamespace = "http://services/", className = "services.GetFilesResponse")
    @Action(input = "http://services/ViewDocumentService/getFilesRequest", output = "http://services/ViewDocumentService/getFilesResponse")
    public List<String> getFiles(
        @WebParam(name = "userName", targetNamespace = "")
        String userName);

}
