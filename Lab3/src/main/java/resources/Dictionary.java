/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package resources;

import java.beans.*;
import java.io.Serializable;
import java.util.ArrayList;

/**
 *
 * @author manol
 */
public class Dictionary implements Serializable {
    
    public static final String PROP_WORDSLIST_PROPERTY = "dictionaryProperty";
    
    public ArrayList<Word> wordsList;
    
    public PropertyChangeSupport propertySupport;
    
    public Dictionary() {
        propertySupport = new PropertyChangeSupport(this);
        wordsList = new ArrayList<>();
    }
    
    public ArrayList<Word> getWordsListProperty() {
        return wordsList;
    }
    
    public void setSampleProperty(ArrayList<Word> value) {
        ArrayList<Word> oldValue = wordsList;
        wordsList = value;
        propertySupport.firePropertyChange(PROP_WORDSLIST_PROPERTY, oldValue, wordsList);
    }
    
    public void addPropertyChangeListener(PropertyChangeListener listener) {
        propertySupport.addPropertyChangeListener(listener);
    }
    
    public void removePropertyChangeListener(PropertyChangeListener listener) {
        propertySupport.removePropertyChangeListener(listener);
    }
    
    private boolean contains(Word w)
    {
        for(Word wordFromList : wordsList)
        {
            if(w.equals(wordFromList))
                return true;
        }
        return false;
    }
    
    public boolean addWord(Word word)
    {
        boolean retVal = false;
        if( !contains(word))
        {
            wordsList.add(word);
            retVal = true;
        }
        return retVal;
    }
    
}
