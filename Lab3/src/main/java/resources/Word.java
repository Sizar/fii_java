/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package resources;

import java.beans.*;
import java.io.Serializable;

/**
 *
 * @author manol
 */
public class Word implements Serializable {
    
    public static final String PROP_WORD_PROPERTY = "wordProperty";
    
    public static final String PROP_DEFINITION_PROPERTY = "definitionProperty";
    
    public static final String PROP_LANGUAGE_PROPERTY = "languageProperty";
    
    private String wordProperty;
    
    private String definitionProperty;
    
    private Language languageProperty;
    
    private PropertyChangeSupport propertySupport;
    
    public Word() {
        propertySupport = new PropertyChangeSupport(this);
    }
    
    public String getWordProperty() {
        return wordProperty;
    }
    
    public String getDefinitionProperty() {
        return definitionProperty;
    }
    
    public Language getLanguageProperty() {
        return languageProperty;
    }
    
    public void setWordProperty(String value) {
        String oldValue = wordProperty;
        wordProperty = value;
        propertySupport.firePropertyChange(PROP_WORD_PROPERTY, oldValue, wordProperty);
    }
    
    public void setDefinitionProperty(String value) {
        String oldValue = definitionProperty;
        definitionProperty = value;
        propertySupport.firePropertyChange(PROP_DEFINITION_PROPERTY, oldValue, definitionProperty);
    }
        
    public void setLanguageProperty(Language value) {
        Language oldValue = languageProperty;
        languageProperty = value;
        propertySupport.firePropertyChange(PROP_LANGUAGE_PROPERTY, oldValue, languageProperty);
    }
    
    public void addPropertyChangeListener(PropertyChangeListener listener) {
        propertySupport.addPropertyChangeListener(listener);
    }
    
    public void removePropertyChangeListener(PropertyChangeListener listener) {
        propertySupport.removePropertyChangeListener(listener);
    }
   
    @Override
    public boolean equals(Object o) { 
  
        // If the object is compared with itself then return true   
        if (o == this) { 
            return true; 
        } 
        /* Check if o is an instance of Complex or not 
          "null instanceof [type]" also returns false */
        if (!(o instanceof Word)) { 
            return false; 
        } 
        Word otherWord = (Word) o;
        return this.definitionProperty.equals(otherWord.getWordProperty()) &&
                this.getLanguageProperty().equals(otherWord.getLanguageProperty()) &&
                this.getDefinitionProperty().equals(otherWord.getDefinitionProperty());
    }
}
