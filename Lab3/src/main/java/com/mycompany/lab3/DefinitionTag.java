package com.mycompany.lab3;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.SimpleTagSupport;
import javax.servlet.jsp.tagext.TagSupport;
import resources.Dictionary;
import resources.Word;


/**
 *
 * @author manol
 */
public class DefinitionTag extends TagSupport {

    private String word;
    

    /**
     * Called by the container to invoke this tag.The implementation of this
 method is provided by the tag library developer, and handles all tag
 processing, body iteration, etc.
     * @throws javax.servlet.jsp.JspException
     */
    @Override
    public int doStartTag() throws JspException {
        JspWriter out = pageContext.getOut();
        
        try {
            HttpServletRequest request = (HttpServletRequest)pageContext.getRequest();
            HttpSession session = request.getSession();
            Dictionary dict = (Dictionary) session.getAttribute("dictionaryProperty");
            boolean found = false;
            if(dict != null)
            {
                for(Word w :dict.getWordsListProperty())
                {
                    if (w.getWordProperty().equals(word))
                    {
                        found = true;
                        out.print("Tag definition for Word: "+ word + " is: " + w.getDefinitionProperty());
                    }
                }
                if (! found)
                {
                    out.print("Tag definition NOT found for word: " + word );
                }
            }
            else
            {
                 out.print("Dictionary bean is null!");
            }
            
        } catch (java.io.IOException ex) {
            throw new JspException("Error in DefinitionTag tag", ex);
        }
        return SKIP_BODY;
    }

    public void setWord(String word) {
        this.word = word;
    }
    public String getWord() {
        return this.word;
    }
    
    
}
