package com.mycompany.lab3;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import java.io.IOException;
import java.io.PrintStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import javax.servlet.DispatcherType;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletRequestWrapper;
import javax.servlet.ServletResponse;
import javax.servlet.ServletResponseWrapper;
import javax.servlet.annotation.WebFilter;
import javax.servlet.annotation.WebInitParam;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpServletResponseWrapper;
import resources.Language;

/**
 *
 * @author manol
 */
@WebFilter(filterName = "InputValidatorFilter", 
        urlPatterns = {"/InputValidatorController"},
        initParams = {
                @WebInitParam(name = "defaultLanguage", value = "RO")
        }
)
public class InputValidatorFilter implements Filter {
    
    private final String WORD_NAME = "WORD_NAME";
    private final String DEFINITION_NAME = "DEFINITION_NAME";
    private final String LANGUAGE_NAME = "LANGUAGE_NAME";
    
    private static final boolean debug = true;

    // The filter configuration object we are associated with.  If
    // this value is null, this filter instance is not currently
    // configured. 
    private FilterConfig filterConfig = null;
    
    public InputValidatorFilter() {
    }    
    
    private boolean isFormFiledInRequest(ServletRequest request)
            throws IOException, ServletException {
        if (debug) {
            // log("InputValidatorFilter:DoBeforeProcessing");
        }
        String word = request.getParameter(WORD_NAME);
        String definition = request.getParameter(DEFINITION_NAME);
        return !(word.isEmpty() || definition.isEmpty());
    }    

    /**
     *
     * @param request The servlet request we are processing
     * @param response The servlet response we are creating
     * @param chain The filter chain we are processing
     *
     * @exception IOException if an input/output error occurs
     * @exception ServletException if a servlet error occurs
     */
    public void doFilter(ServletRequest request, ServletResponse response,
            FilterChain chain)
            throws IOException, ServletException {
        
        if (debug) {
            log("InputValidatorFilter:doFilter()");
        }
        
        if(!isFormFiledInRequest(request))
        {
            ((HttpServletResponse)response).sendRedirect("input.jsp");
        }
        else
        {
            // Decorate the response 
            Throwable problem = null;
            try {
                if (!request.getParameterMap().keySet().contains(LANGUAGE_NAME))
                {
                    // Decorating the request
                    ServletRequestWrapper  requestDecorater = new ServletRequestWrapper(request);
                    requestDecorater.setAttribute(LANGUAGE_NAME, filterConfig.getServletContext().getInitParameter("defaultLanguage"));
                    chain.doFilter(requestDecorater, response);
                }
                else
                {
                    chain.doFilter(request, response);
                }
            } catch (IOException | ServletException t) {
                problem = t;
                t.printStackTrace();
            }
            
            if (problem != null) {
                if (problem instanceof ServletException) {
                    throw (ServletException) problem;
                }
                if (problem instanceof IOException) {
                    throw (IOException) problem;
                }
                sendProcessingError(problem, response);
            }
        }
    }

    /**
     * Return the filter
     * @return configuration object for this filter.
     */
    public FilterConfig getFilterConfig() {
        return (this.filterConfig);
    }

    /**
     * Set the filter configuration object for this filter.
     *
     * @param filterConfig The filter configuration object
     */
    public void setFilterConfig(FilterConfig filterConfig) {
        this.filterConfig = filterConfig;
    }

    /**
     * Destroy method for this filter
     */
    public void destroy() {        
    }

    /**
     * Init method for this filter
     * @param filterConfig
     */
    @Override
    public void init(FilterConfig filterConfig) {        
        this.filterConfig = filterConfig;
        if (filterConfig != null) {
            if (debug) {                
                log("InputValidatorFilter:Initializing filter");
            }
        }
    }

    /**
     * Return a String representation of this object.
     */
    @Override
    public String toString() {
        if (filterConfig == null) {
            return ("InputValidatorFilter()");
        }
        StringBuilder sb = new StringBuilder("InputValidatorFilter(");
        sb.append(filterConfig);
        sb.append(")");
        return (sb.toString());
    }
    
    private void sendProcessingError(Throwable t, ServletResponse response) {
        String stackTrace = getStackTrace(t);        
        
        if (stackTrace != null && !stackTrace.equals("")) {
            try {
                response.setContentType("text/html");
                PrintStream ps = new PrintStream(response.getOutputStream());
                PrintWriter pw = new PrintWriter(ps);                
                pw.print("<html>\n<head>\n<title>Error</title>\n</head>\n<body>\n"); //NOI18N

                // PENDING! Localize this for next official release
                pw.print("<h1>The resource did not process correctly</h1>\n<pre>\n");                
                pw.print(stackTrace);                
                pw.print("</pre></body>\n</html>"); //NOI18N
                pw.close();
                ps.close();
                response.getOutputStream().close();
            } catch (Exception ex) {
            }
        } else {
            try {
                PrintStream ps = new PrintStream(response.getOutputStream());
                t.printStackTrace(ps);
                ps.close();
                response.getOutputStream().close();
            } catch (Exception ex) {
            }
        }
    }
    
    public static String getStackTrace(Throwable t) {
        String stackTrace = null;
        try {
            StringWriter sw = new StringWriter();
            PrintWriter pw = new PrintWriter(sw);
            t.printStackTrace(pw);
            pw.close();
            sw.close();
            stackTrace = sw.getBuffer().toString();
        } catch (Exception ex) {
        }
        return stackTrace;
    }
    
    public void log(String msg) {
        filterConfig.getServletContext().log(msg);        
    }
    
}
