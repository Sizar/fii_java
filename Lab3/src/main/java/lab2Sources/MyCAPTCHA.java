/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lab2Sources;

import java.io.IOException;
import java.util.Iterator;
import java.util.Random;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author manol
 */
@WebServlet(name = "MyCAPTCHA", urlPatterns = {"/MyCAPTCHA"})
public class MyCAPTCHA extends HttpServlet {

    private String GENERATED_WORD_NAME = "word_captcha_to_match";
    private String REQUEST_WORD_NAME = "word_request_captcha";

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        response.setContentType("text/html;charset=UTF-8");
        
        //printReqInfo(request);
        
        HttpSession session = request.getSession(false);
        session.setMaxInactiveInterval(30 * 60);
        String generatedWord = wordGenerator();
        //System.out.print("com.mycompany.lab2.MyCAPTCHA.wordGenerator(): " + generatedWord);
        session.setAttribute(GENERATED_WORD_NAME, generatedWord);
        StringBuilder sb = new StringBuilder();
            
            //sb.append("<h1>Servlet MyCAPTCHA at ").append(request.getContextPath()).append("</h1>");
            sb.append("<br><br>");
            sb.append("<canvas id=\"myCanvas\" width=\"200\" height=\"100\"");
            sb.append("style=\"border:1px solid #d3d3d3;\">");
            sb.append("Your browser does not support the canvas element.");
            sb.append("</canvas>");
            
            sb.append("<script>");
            sb.append("var canvas = document.getElementById(\"myCanvas\")\n");
            sb.append("var ctx = canvas.getContext(\"2d\")\n");
            sb.append("ctx.font = \"30px Arial\"\n");
            sb.append("ctx.fillText(\"").append(generatedWord).append("\",10,50)\n");
            sb.append("</script>");
            
            sb.append("<br><br>");
            sb.append("<label for=\"fname\">Write here:</label>" );
            sb.append("<input type=\"text\" id=\"").append(REQUEST_WORD_NAME).append("\" name=\"").append(REQUEST_WORD_NAME).append("\" contenteditable=\"false\"> ");
            sb.append("<br><br>");
           
            
        request.setAttribute("MyCAPTCHA", sb.toString());
        
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        response.setContentType("text/html;charset=UTF-8");
        HttpSession session = request.getSession(false);
        if (session == null)
        {
            throw new IOException("WHAT?!?!!");
        }
        String wordToMatch = (String)session.getAttribute(GENERATED_WORD_NAME);
        String wordFromRequest = request.getParameter(REQUEST_WORD_NAME);
        System.out.println("com.mycompany.lab2.MyCAPTCHA.doPost(): Word to match: " + wordToMatch + " to match: " + wordFromRequest);
        
        if(wordToMatch.equals(wordFromRequest))
        {   
            //request.setAttribute("CAPTCHA_PASSED", "1");
        }
        else
        {
            RequestDispatcher dispatcher = getServletConfig().getServletContext().getRequestDispatcher("/error.jsp");
            Integer errorCode = 400;
            String s = "Captha word dont match!";
            IOException e = new IOException("Captha word dont match");
            request.setAttribute("javax.servlet.error.status_code", errorCode);
            request.setAttribute( "javax.servlet.error.message", s);
            request.setAttribute("javax.servlet.error.exception", e);
            dispatcher.forward(request, response);
        }
    }
    private String wordGenerator()
    {
        StringBuilder sb = new StringBuilder();
        Random r = new Random();
        int length = r.nextInt(2)+5;
        for(int i = 0; i < length; i++)
        {
            if(r.nextBoolean())
            {
                sb.append((char)('0'+ r.nextInt('9'-'0')) );
            }
            else
            {
                if(r.nextBoolean())
                {
                    sb.append((char)('a' + r.nextInt('z'-'a')));
                }
                else
                {
                    sb.append((char)('A' + r.nextInt('Z'-'A')));
                }
            }
        }
        // System.out.print("com.mycompany.lab2.MyCAPTCHA.wordGenerator(): " + sb.toString());
        return sb.toString();
    }
    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
