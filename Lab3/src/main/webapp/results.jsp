<%-- 
    Document   : results
    Created on : Oct 21, 2020, 5:12:53 PM
    Author     : manol
--%>

<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@page import="resources.Dictionary"%>
<%@page import="resources.Word"%>
<%@page import="com.mycompany.lab3.DefinitionTag"%>
<%@page contentType="text/html" pageEncoding="UTF-8" errorPage="error.jsp"%>
<%@taglib uri='/WEB-INF/tlds/mylibrary' prefix="get"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="get" uri="/WEB-INF/tlds/mylibrary"%>" %>
<%@ taglib tagdir="/WEB-INF/tags" prefix="tf" %>

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP RESLT Page</title>
        <style>
        table {
            font-family: arial, sans-serif;
            border-collapse: collapse;
            width: 100%;
        }

        td, th {
            border: 1px solid #dddddd;
            text-align: left;
            padding: 8px;
        }

        tr:nth-child(even) {
            background-color: #dddddd;
        }
</style>
    </head>
    <body>
        <%
        HttpSession sesion = request.getSession();
        Dictionary dictionary = (Dictionary) sesion.getAttribute(Dictionary.PROP_WORDSLIST_PROPERTY);

        if ( dictionary == null)
        {
            throw new Exception("The dictionary was empty at result");
        } 
        %>
         <table id="sampTable">
             <tr>
                <th>WORD</th>
                <th>DEFINITION</th>
                <th>LANGUAGE</th>
            </tr>
        <% for(Word w : dictionary.getWordsListProperty())
        {
                out.print("<tr>");    
                out.print("<th> " + w.getWordProperty()+ "</th>" 
                        + "<th> " + w.getDefinitionProperty()+"</th>" 
                        + "<th> " + w.getLanguageProperty().toString()+"</th>");  
                out.print("</tr>");
        }
        %>  
       
        </table> 
        
        <label id="DefinitionTag">
            <get:definition word="Student"/>
        </label>
        <br>
        <tf:viewDictionary language="RO"/>
        
        
        
        
        <form action="input.jsp" method="GET">
          
            <input type="submit" value="Back to Input" />
        </form>
        
    </body>
</html>
