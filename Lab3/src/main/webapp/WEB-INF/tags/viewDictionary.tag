<%-- 
    Document   : viewDictionary
    Created on : Nov 3, 2020, 12:21:27 PM
    Author     : manol
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ tag description="put the tag description here" pageEncoding="UTF-8"%>
<%@ attribute name="language" required="true" %>

<center>
<table id="table_tag">
    <tr>
        <th>WORD_Jstl</th>
        <th>DEFINITION_Jstl</th>
    </tr>
<c:forEach var="word" items="${sessionScope.dictionaryProperty.getWordsListProperty()}">
    <c:choose>
        <c:when test="${word.getLanguageProperty().toString() == language}">
            <tr>
            <td>
                <c:out value="${word.getWordProperty()}" />
            </td>
            <td>
                <c:out value="${word.getDefinitionProperty()}" />
           </td>
            </tr>
        </c:when>
    </c:choose>
</c:forEach>
</table>
</center>