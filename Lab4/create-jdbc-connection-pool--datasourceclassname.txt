create-jdbc-connection-pool 
--datasourceclassname org.postgresql.ds.PGSimpleDataSource
--restype  javax.sql.DataSource --driverclassname=org.postgresql.Driver 
--property serverName=localhost:portNumber=5432:databaseName=sample:user=dba:password=sql SamplePool 
create-jdbc-resource --connectionpoolid SamplePool jdbc/sample