/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package services;

import java.util.Date;
import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.Schedule;
import javax.ejb.Stateless;
import javax.ejb.Timeout;
import javax.ejb.Timer;
import javax.ejb.TimerService;

/**
 *
 * @author manol
 */
@Stateless
public class TimerSessionBean {
    @Resource
    TimerService timerService;
    
    @EJB
    Pers_to_meetService pers_to_meetService;
    @EJB
    StateService stateService;
    
    public void createTimer(long milliseconds) 
    {
        Date timeout = new Date(new Date().getTime() + milliseconds);
        timerService.createTimer(timeout, "Hello World!");
    }
    
    @Timeout
    public void timeoutHandler(Timer timer) 
    {
        pers_to_meetService.list();
        stateService.getPers_to_meet();
    }

    //or simply
    @Schedule(second="*/1", minute="*",hour="*", persistent=false)
    public void doWork()
    {
        pers_to_meetService.list();
        stateService.getPers_to_meet();
    }  
}
