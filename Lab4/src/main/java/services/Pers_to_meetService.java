/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package services;

import entitys.Location;
import entitys.Meet_to_loc;
import entitys.Meeting;
import entitys.Pers_to_meet;
import java.util.List;
import javax.ejb.Lock;
import static javax.ejb.LockType.READ;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.interceptor.AroundInvoke;
import javax.interceptor.InvocationContext;
import javax.persistence.Cacheable;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author manol
 */
@Cacheable(true)
@Stateless
public class Pers_to_meetService {
    @PersistenceContext
    private EntityManager em;
 
    public List<Pers_to_meet> list() {
        return em.createNamedQuery("getPersToMeet").getResultList();
    }

    public Pers_to_meet find(Integer id) {
        return em.find(Pers_to_meet.class, id);
    }
    
    public boolean exists(Integer id) {
        return em.find(Pers_to_meet.class, id) != null;
    }

    @TransactionAttribute(TransactionAttributeType.MANDATORY)
    public Integer save(Pers_to_meet ptm) {
        em.persist(ptm);
        return ptm.getId();
    }

    public void update(Pers_to_meet ptm) {
        em.merge(ptm);
    }

    public void delete(Pers_to_meet ptm) {
        em.remove(em.contains(ptm) ? ptm : em.merge(ptm));
    }
    
    @AroundInvoke
    public Object log (InvocationContext ctx) throws Exception 
    {
        String className = ctx.getTarget().getClass().getName();
        String methodName = ctx.getMethod().getName();
        String target = className + "." + methodName + "()";
        long t1 = System.currentTimeMillis();
        try {
            return ctx.proceed();
        } 
        catch(Exception e) 
        {
            throw e;
        } 
        finally 
        {
            long t2 = System.currentTimeMillis();
            System.out.println(target + " took " + (t2-t1) + "ms to execute");
        }
    }
}
