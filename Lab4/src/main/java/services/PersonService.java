/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package services;

import entitys.Person;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author manol
 */
@Stateless
public class PersonService  {
    
    @PersistenceContext
    private EntityManager em;

    public List<Person> list() {
        return em.createNamedQuery("getPersons").getResultList();
    }

    public Person find(Integer id) {
        return em.find(Person.class, id);
    }

    public boolean find(String name) {
        for(Person p : list())
        {
            if(p.getName().equals(name))
            {
                return true;
            }
        }
        return false;
    }
    
    public int find_id(String name) {
        for(Person p : list())
        {
            if(p.getName().equals(name))
            {
                return p.getId();
            }
        }
        return -1;
    }
    
    public Person find_pers(String name) {
        for(Person p : list())
        {
            if(p.getName().equals(name))
            {
                return p;
            }
        }
        return null;
    }
    public Integer save(Person person) {
        em.persist(person);
        return person.getId();
    }

    public void update(Person person) {
        em.merge(person);
    }

    public void delete(Person person) {
        em.remove(em.contains(person) ? person : em.merge(person));
    }
}
