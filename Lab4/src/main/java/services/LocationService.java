/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package services;

import entitys.Location;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author manol
 */
@Stateless
public class LocationService {
    @PersistenceContext
    private EntityManager em;
 
    public List<Location> list() {
        return em.createNamedQuery("getLocations").getResultList();
    }

    public Location find(Integer id) {
        return em.find(Location.class, id);
    }

     public boolean find(String name) {
        for(Location l : list())
        {
            if(l.getName().equals(name))
            {
                return true;
            }
        }
        return false;
    }
     
    public Integer save(Location location) {
        em.persist(location);
        return location.getId();
    }

    public void update(Location location) {
        em.merge(location);
    }

    public void delete(Location location) {
        em.remove(em.contains(location) ? location : em.merge(location));
    }
}
