/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package services;

import entitys.Location;
import entitys.Meet_to_loc;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author manol
 */
@Stateless
public class Meet_to_locService {
    @PersistenceContext
    private EntityManager em;
 
    public List<Location> list() {
        return em.createNamedQuery("getMeetToLoc").getResultList();
    }

    public Meet_to_loc find(Integer id) {
        return em.find(Meet_to_loc.class, id);
    }

      
    public Integer save(Meet_to_loc mtl) {
        em.persist(mtl);
        return mtl.getId();
    }

    public void update(Meet_to_loc mtl) {
        em.merge(mtl);
    }

    public void delete(Meet_to_loc mtl) {
        em.remove(em.contains(mtl) ? mtl : em.merge(mtl));
    }
}
