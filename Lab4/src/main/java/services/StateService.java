/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package services;

import javax.ejb.Singleton;
import entitys.Pers_to_meet;
import java.io.Serializable;

/**
 *
 * @author manol
 */
import java.util.List;
import javax.annotation.PostConstruct;
import javax.ejb.ConcurrencyManagement;
import javax.ejb.Lock;
import static javax.ejb.ConcurrencyManagementType.CONTAINER;
import javax.ejb.EJB;
import static javax.ejb.LockType.READ;
import static javax.ejb.LockType.WRITE;
import javax.ejb.MessageDriven;
import javax.interceptor.AroundInvoke;
import javax.interceptor.InvocationContext;
import javax.jms.Message;
import javax.jms.MessageListener;


@Singleton
@ConcurrencyManagement(CONTAINER)
public class StateService {
    @EJB
    private Pers_to_meetService serice;
    
    //Chache in-memory mapp
    private List<Pers_to_meet> pers_to_meet;
    
    @PostConstruct
    void init()
    {
        pers_to_meet = serice.list();
    }
    
    @Lock(READ)
    public List<Pers_to_meet> getPers_to_meet() 
    {
        System.out.println("services.AssignmentStateService.getPers_to_meet()");
        return pers_to_meet;
    }
    
    @Lock(WRITE)
    public void update() {
        System.out.println("services.AssignmentStateService.update()");
        pers_to_meet = serice.list();
    }
    
    @AroundInvoke
    @Lock(READ)
    public Object log (InvocationContext ctx) throws Exception 
    {
        String className = ctx.getTarget().getClass().getName();
        String methodName = ctx.getMethod().getName();
        String target = className + "." + methodName + "()";
        long t1 = System.currentTimeMillis();
        try {
            return ctx.proceed();
        } 
        catch(Exception e) 
        {
            throw e;
        } 
        finally 
        {
            long t2 = System.currentTimeMillis();
            System.out.println(target + " took " + (t2-t1) + "ms to execute");
        }
    }
}
