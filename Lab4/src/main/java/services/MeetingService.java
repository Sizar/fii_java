/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package services;

import bean.pack.MeetingBean;
import entitys.Location;
import entitys.Meet_to_loc;
import entitys.Meeting;
import entitys.Pers_to_meet;
import entitys.Person;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Set;
import java.util.function.Function;
import javax.ejb.Stateless;
import javax.interceptor.AroundInvoke;
import javax.interceptor.InvocationContext;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TemporalType;
import javax.persistence.Tuple;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Expression;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.ParameterExpression;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.persistence.criteria.SetJoin;
import metamodels.Location_;
import metamodels.Meet_to_loc_;
import metamodels.Meeting_;
import metamodels.Pers_to_met_;
import metamodels.Person_;

/**
 *
 * @author manol
 */
@Stateless
public class MeetingService {
   
    @PersistenceContext
    private EntityManager em;
    
    public List<Meeting> list() {
        return em.createNamedQuery("getMeetings").getResultList();
    }

    public Meeting find(Integer id) {
        return em.find(Meeting.class, id);
    }
     
    public Integer save(Meeting meeting) {
        em.persist(meeting);
        return meeting.getId();
    }

    public void update(Meeting meeting) {
        em.merge(meeting);
    }

    public void delete(Meeting meeting) {
        em.remove(em.contains(meeting) ? meeting : em.merge(meeting));
    }
    
    private List<MeetingBean> toBeanList(List<Meeting> list)
    {
       List<MeetingBean> retVal = new ArrayList<>();
       for(Meeting m : list)
       {
            MeetingBean meeting = new MeetingBean();
            meeting.setId(m.getId());
            meeting.setStartTime( new java.util.Date(m.getStartTime().getTime()));
            meeting.setDuration(m.getDuration());
            meeting.setTopic(m.getTopic());
            retVal.add(meeting);
            
       }
       return retVal;
    }
    
    public List<MeetingBean> search(
            String location,
            boolean locationNameChecked,
            String person,
            boolean personNameChecked,
            Date startTime,
            boolean startTimeChecked)
    {
       List<Meeting> retVal = list();
       if (locationNameChecked)
       {
           List<Meeting> retFromFunction = getMeetingsFromLocation(location); 
           List<Meeting> gold_collection = new ArrayList<>();
           for(Meeting m : retVal)
           {
                if(retFromFunction.contains(m))
                {
                    gold_collection.add(m);
                }
           }
           retVal = gold_collection;
       }
       if (personNameChecked)
       {
           List<Meeting> retFromFunction = getMeetingsForPerson(person);
           List<Meeting> gold_collection = new ArrayList<>();
           for(Meeting m : retVal)
           {
                if(retFromFunction.contains(m))
                {
                    gold_collection.add(m);
                }
           }
           retVal = gold_collection;
       }
       if (startTimeChecked)
       {
           List<Meeting> retFromFunction = getMeetingsStartingFrom(startTime);
           List<Meeting> gold_collection = new ArrayList<>();
           for(Meeting m : retVal)
           {
                if(retFromFunction.contains(m))
                {
                    gold_collection.add(m);
                }
           }
           retVal = gold_collection;
       }
       return toBeanList(retVal);
    }
    
    private List<Meeting> getMeetingsStartingFrom(Date startTime)
    {
        CriteriaBuilder builder = em.getCriteriaBuilder();
        CriteriaQuery<Meeting> query = builder.createQuery(Meeting.class);
        Root<Meeting> meetingRoot = query.from(em.getMetamodel().entity(Meeting.class));
        
        ParameterExpression<java.util.Date> parameter = builder.parameter(java.util.Date.class);
        Predicate startDatePredicate = builder.greaterThanOrEqualTo(meetingRoot.get(Meeting_.startTime).as(java.sql.Date.class), parameter);
        Predicate startDateOrPredicate = builder.or(startDatePredicate, meetingRoot.get(Meeting_.startTime).isNull());    
        
        query.where(startDateOrPredicate);
        
        List<Meeting> retVal = em.createQuery(query)
                .setParameter(parameter, startTime, TemporalType.DATE).getResultList();
        
        
        return retVal;
    }

     private Person getPerson(String personName)
    {
        CriteriaBuilder builder = em.getCriteriaBuilder();
        CriteriaQuery<Person> query = builder.createQuery(Person.class);
        Root<Person> root = query.from(em.getMetamodel().entity(Person.class));
        
        
        ParameterExpression<String> parameter_name = builder.parameter(String.class);
        Predicate personInMeeting = builder.like(root.get(Person_.name), parameter_name);
        
        query.where(personInMeeting);
        
        List<Person> list_to_filter = em.createQuery(query)
                .setParameter(parameter_name, personName).getResultList();
        
        
        return list_to_filter.size() >0 ? list_to_filter.get(0) : null;
    }
    
    private List<Meeting> getMeetingsForPerson(String personName)
    {
        CriteriaBuilder builder = em.getCriteriaBuilder();
        CriteriaQuery<Pers_to_meet> query = builder.createQuery(Pers_to_meet.class);
        Root<Pers_to_meet> root = query.from(em.getMetamodel().entity(Pers_to_meet.class));
        
        
        ParameterExpression<Person> parameter_person = builder.parameter(Person.class);
        Predicate personInMeeting = builder.equal(root.get(Pers_to_met_.person), parameter_person);
        
        query.where(personInMeeting);
        
        List<Pers_to_meet> list_to_filter = em.createQuery(query)
                .setParameter(parameter_person, getPerson(personName) ).getResultList();
        
        List<Meeting> retVal = new ArrayList<>();
        
        for(Pers_to_meet ptm : list_to_filter)
        {
            retVal.add(ptm.getMeeting());
        }
        
        return retVal;
    }
    
    private Location getLocation(String locationName)
    {
        CriteriaBuilder builder = em.getCriteriaBuilder();
        CriteriaQuery<Location> query = builder.createQuery(Location.class);
        Root<Location> root = query.from(em.getMetamodel().entity(Location.class));
        
        
        ParameterExpression<String> parameter_location = builder.parameter(String.class);
        Predicate personInMeeting = builder.like(root.get(Location_.name), parameter_location);
        
        query.where(personInMeeting);
        
        List<Location> list_to_filter = em.createQuery(query)
                .setParameter(parameter_location, locationName).getResultList();
        
        
        return list_to_filter.size() >0 ? list_to_filter.get(0) : null;
    }
    
    private List<Meeting> getMeetingsFromLocation(String locationName)
    {
        CriteriaBuilder builder = em.getCriteriaBuilder();
        CriteriaQuery<Meet_to_loc> query = builder.createQuery(Meet_to_loc.class);
        Root<Meet_to_loc> root = query.from(em.getMetamodel().entity(Meet_to_loc.class));
        
        ParameterExpression<Location> parameter_location = builder.parameter(Location.class);
        Predicate personInMeeting = builder.equal(root.get(Meet_to_loc_.location), parameter_location);
        
        query.where(personInMeeting);
        
        List<Meet_to_loc> list_to_filter = em.createQuery(query)
                .setParameter(parameter_location, getLocation(locationName)).getResultList();
        
        List<Meeting> retVal = new ArrayList<>();
        
        for(Meet_to_loc ptm : list_to_filter)
        {
            retVal.add(ptm.getMeeting());
        }
        
        return retVal;
    }
    
}
