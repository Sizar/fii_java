/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package services;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import entitys.Meeting;
import entitys.Pers_to_meet;
import entitys.Person;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.ParameterExpression;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import metamodels.Pers_to_met_;

/**
 *
 * @author manol
 */
@Stateless
public class AvailabilityService {
    @PersistenceContext
    private EntityManager em;
    
    
    @TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
    public int getAvailableSpacesForMeeting(int meetId)
    {
        Meeting meeting = em.find(Meeting.class, meetId);
        CriteriaBuilder builder = em.getCriteriaBuilder();
        CriteriaQuery<Pers_to_meet> query = builder.createQuery(Pers_to_meet.class);
        Root<Pers_to_meet> root = query.from(em.getMetamodel().entity(Pers_to_meet.class));
        
        
        ParameterExpression<Meeting> parameter_meeting = builder.parameter(Meeting.class);
        Predicate personInMeeting = builder.equal(root.get(Pers_to_met_.meeting), parameter_meeting);
        
        query.where(personInMeeting);
        
        List<Pers_to_meet> list_to_filter = em.createQuery(query)
                .setParameter(parameter_meeting, meeting ).getResultList();
        // System.out.println(" return:" + (meeting.getMax_pers() - list_to_filter.size()));
        return meeting.getMax_pers() - list_to_filter.size();
    }
    
    @TransactionAttribute(TransactionAttributeType.MANDATORY)
    public boolean canAddPersToMeet(int meetId)
    {
        Meeting m = em.find(Meeting.class, meetId);
        return m.getMax_pers() - m.pers_to_meet.size() > 0;
    }
}
