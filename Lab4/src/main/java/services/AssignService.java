/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package services;

import entitys.Pers_to_meet;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Remove;
import javax.ejb.Stateful;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.enterprise.inject.spi.Producer;
import javax.jms.ConnectionFactory;
import javax.jms.Destination;
import javax.jms.JMSContext;
import javax.jms.JMSProducer;
import javax.jms.JMSSessionMode;
import javax.jms.Message;
import javax.jms.MessageProducer;
import javax.jms.Queue;
import javax.jms.QueueBrowser;
import javax.jms.Session;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author manol
 */
@Stateful
@LocalBean
public class AssignService {
    @PersistenceContext
    private EntityManager em;
    
    @EJB
    AvailabilityService availabilityService;
    @EJB
    StateService stateService;
    
    List<Pers_to_meet> listToAdd;
    
    @PostConstruct
    public void init() 
    {
        listToAdd = new ArrayList<>();
    }
    
    @TransactionAttribute(TransactionAttributeType.NEVER)
    public void addItem(Pers_to_meet item) 
    {
        listToAdd.add(item);
    }
    
    public List<Pers_to_meet> getContents() 
    {
        return listToAdd;
    }
    
     public boolean  containsId(Integer id) 
    {
        boolean retVal = false;
        for(Pers_to_meet ptm : listToAdd)
        { 
            if (ptm.getId() == id)
                retVal = true;
        }
        return retVal;
    }
    
    @Remove
    public void save() throws Exception 
    {
        System.out.println("Saving ... \n" );
        for(Pers_to_meet ptm : listToAdd)
        { 
            System.out.println(ptm.getMeeting().getTopic() +" " +  availabilityService.getAvailableSpacesForMeeting(ptm.getMeeting().getId()));
            if (availabilityService.getAvailableSpacesForMeeting(ptm.getMeeting().getId()) > 0)
            {
                em.persist(ptm);
                System.out.println("Saving ... -> " + ptm.getId());
                em.flush();
            }
            else
            {
                throw new Exception("The limt for meeting " + ptm.getMeeting().getTopic() +" was reached!");
            }
        }
        
        stateService.update();
    }
}
