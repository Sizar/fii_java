/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entitys;

import java.io.Serializable;
import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

/**
 *
 * @author manol
 */

@NamedQuery(name="getLocations",query="SELECT l FROM Location l")
@Entity
@Table(name = "LOCATIONS")
public class Location implements Serializable {
    
    @Id
    @Column(name = "ID")
    private Integer id;
    
    @Column(name = "name")
    private String name;
    
    
    @OneToMany( cascade = CascadeType.ALL, mappedBy = "location" )
    public Set<Meet_to_loc> meet_to_loc;
    
    public Integer getId()
    {
        return id;
    }
    
    public void setId(Integer id)
    {
        this.id = id;
    }
    
    public String getName()
    {
        return name;
    }
    
    public void setName(String name)
    {
        this.name = name;
    }
}
