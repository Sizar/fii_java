/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entitys;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MapsId;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

/**
 *
 * @author manol
 */
@NamedQuery(name="getPersToMeet",query="SELECT ptm FROM Pers_to_meet ptm")
@Entity
@Table(name = "PERS_TO_MEET")
public class Pers_to_meet implements Serializable {
    @Id
    @Column(name = "id")
    private int id;
    
    @ManyToOne
    @MapsId("id")
    @JoinColumn(name = "id_person")
    private Person person;
    
    @ManyToOne
    @MapsId("id")
    @JoinColumn(name = "id_meeting")
    private Meeting meeting;

    public int getId()
    {
        return this.id;
    }
    
    public void setId(int id)
    {
        this.id = id;
    }
    
    public Person getPerson()
    {
        return this.person;
    }
    
    public void setPerson(Person person)
    {
        this.person = person;
    }
    
    public Meeting getMeeting()
    {
        return this.meeting;
    }
    
    public void setMeeting(Meeting meeting)
    {
        this.meeting = meeting;
    } 
}
