/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entitys;

import java.io.Serializable;
import java.sql.Date;
import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinTable;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

/**
 *
 * @author manol
 */
@NamedQuery(name="getMeetings",query="SELECT m FROM Meeting m")
@Entity
@Table(name = "MEETINGS")
public class Meeting implements Serializable {
    
    @Id 
    @Column(name = "id")
    private int id;
    
    @Column(name = "topic")
    private String topic;
    
    @Column(name = "start_time")
    private Date startTime;
    
    @Column(name = "duration")
    private int duration;    
    
    @Column(name = "max_pers")
    private int max_pers;
    
    @OneToMany( cascade = CascadeType.ALL, mappedBy = "meeting" )
    public Set<Meet_to_loc> meet_to_loc;
    
    @OneToMany( cascade = CascadeType.ALL, mappedBy = "meeting" )
    @JoinTable(name="PERS_TO_MEET")
    public Set<Pers_to_meet> pers_to_meet;
    
    
    public int getId()
    {
        return this.id;
    }
    
    public void setId(int id)
    {
        this.id = id;
    }
    
    public String getTopic()
    {
        return topic;
    }
    
    public void setTopic(String topic)
    {
        this.topic = topic;
    }
    public void setStartTime(Date startTime)
    {
        this.startTime = new Date(startTime.getTime());
    }
    
    public Date getStartTime()
    {
        return startTime;
    }
        
    public void setDuration(int duration)
    {
        this.duration = duration;
    }
    
    public int getDuration()
    {
        return duration;
    }
    
    public void setMax_pers(int max_pers)
    {
        this.max_pers = max_pers;
    }
    
    public int getMax_pers()
    {
        return max_pers;
    }
}
