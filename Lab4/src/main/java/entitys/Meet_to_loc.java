/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entitys;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MapsId;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

/**
 *
 * @author manol
 */
@NamedQuery(name="getMeetToLoc",query="SELECT mtl FROM Meet_to_loc mtl")
@Entity
@Table(name = "MEET_TO_LOC")
public class Meet_to_loc implements Serializable {
    @Id
    @Column(name = "id")
    private int id;
    
    @ManyToOne@MapsId("id")
    @JoinColumn(name = "id_meeting")
    private Meeting meeting;

    @ManyToOne
    @MapsId("id")
    @JoinColumn(name = "id_location")
    private Location location;
    
    
    public int getId()
    {
        return this.id;
    }
    
    public void setId(int id)
    {
        this.id = id;
    }
    
    
    public Location getLocation()
    {
        return this.location;
    }
    
    public void setLocation(Location location)
    {
        this.location = location;
    }
    
    public Meeting getMeeting()
    {
        return this.meeting;
    }
    
    public void setMeeting(Meeting meeting)
    {
        this.meeting = meeting;
    }
}