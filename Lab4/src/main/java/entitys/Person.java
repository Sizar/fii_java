/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entitys;

import java.io.Serializable;
import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinTable;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

/**
 *
 * @author manol
 */
@NamedQuery(name="getPersons",query="SELECT p FROM Person p")
@Entity
@Table(name = "persons")
public class Person implements Serializable {
    
    @Id
    @SequenceGenerator(name = "sequencen", sequenceName = "persons_id_seq")
    @GeneratedValue(generator = "sequencen")
    @Column(name = "ID")
    private int id;
    
    
    @Column(name = "name")
    private String name;
    
    @OneToMany( cascade = CascadeType.ALL, mappedBy = "person" )
    @JoinTable(name="PERS_TO_MEET")
    public Set<Pers_to_meet> pers_to_meet;
    
    public Integer getId()
    {
        return id;
    }
    
    public void setId(Integer id)
    {
        this.id = id;
    }
    
    public String getName()
    {
        return name;
    }
    
    public void setName(String name)
    {
        this.name = name;
    }
}
