/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package metamodels;

import entitys.Location;
import entitys.Meeting;
import entitys.Person;
import java.sql.Date;
import java.util.List;
import javax.persistence.metamodel.SingularAttribute;

/**
 *
 * @author manol
 */
@javax.persistence.metamodel.StaticMetamodel(Person.class)
public class Person_ {
    public static volatile SingularAttribute<Person, String> name;
}
