/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package metamodels;

import entitys.Location;
import entitys.Meet_to_loc;
import entitys.Meeting;
import entitys.Pers_to_meet;
import entitys.Person;
import java.sql.Date;
import java.util.List;
import java.util.Set;
import javax.persistence.metamodel.SingularAttribute;

/**
 *
 * @author manol
 */
@javax.persistence.metamodel.StaticMetamodel(Meeting.class)
public class Meeting_ {
    public static volatile SingularAttribute<Meeting, Date> startTime;
    public static volatile javax.persistence.metamodel.ListAttribute<Meeting, List<Pers_to_meet>> pers_to_meet;
    // public static volatile javax.persistence.metamodel.ListAttribute<Meeting, List<Meet_to_loc>> meet_to_loc;
}
