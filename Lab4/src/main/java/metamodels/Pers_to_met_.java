/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package metamodels;

import entitys.Meeting;
import entitys.Pers_to_meet;
import entitys.Person;
import javax.persistence.metamodel.SingularAttribute;

/**
 *
 * @author manol
 */
@javax.persistence.metamodel.StaticMetamodel(Pers_to_meet.class)
public class Pers_to_met_ {
    public static volatile SingularAttribute<Pers_to_meet, Person> person;
    public static volatile SingularAttribute<Pers_to_meet, Meeting> meeting;
}
