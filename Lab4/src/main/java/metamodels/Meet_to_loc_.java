/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package metamodels;

import entitys.Location;
import entitys.Meet_to_loc;
import entitys.Meeting;
import entitys.Pers_to_meet;
import entitys.Person;
import javax.persistence.metamodel.SingularAttribute;

/**
 *
 * @author manol
 */
@javax.persistence.metamodel.StaticMetamodel(Meet_to_loc.class)
public class Meet_to_loc_ {
    
    public static volatile SingularAttribute<Meet_to_loc, Location> location;
    public static volatile SingularAttribute<Meet_to_loc, Meeting> meeting;
}
