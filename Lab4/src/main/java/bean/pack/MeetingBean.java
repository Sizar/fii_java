/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bean.pack;

import entitys.Location;
import entitys.Meeting;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.enterprise.context.RequestScoped;
import javax.inject.Named;
import javax.sql.DataSource;
import services.LocationService;
import services.MeetingService;

/**
 *
 * @author manol
 */
@Named("meetingBean")
@RequestScoped
public class MeetingBean {
    
    private Meeting meeting;
    private List<Meeting> meetings;

    @EJB
    private MeetingService service;
    
    @PostConstruct
    public void init() {
        meeting = new Meeting();
        meetings = service.list();
    }
    
    private int id;
    private String topic;
    private Date startTime;
    private int duration;
    private int max_pers;
    
    public MeetingBean(){
        System.out.println("bean.pack.MeetingBean.<init1>()");
    }
    
    public MeetingBean(int id,
     String topic,
     Date startTime,
     int duration)
    {
        System.out.println("bean.pack.MeetingBean.<init2>()");
        this.id = id;
        this.topic = topic;
        this.startTime = startTime;
        this.duration = duration;
    }
    
    public void setTopic(String topic)
    {
        this.topic = topic;
    }
    
    public String getTopic()
    {
        return topic;
    }
    
    public void setStartTime(Date startTime)
    {
        this.startTime = new Date(startTime.getTime());
    }
    
    public Date getStartTime()
    {
        return startTime;
    }
        
    public void setDuration(int duration)
    {
        this.duration = duration;
    }
    
    public int getDuration()
    {
        return duration;
    }
    
    public int getMax_pers()
    {
        return max_pers;
    }
    
    public void setMax_pers(int max_pers)
    {
        this.max_pers = max_pers;
    }
    
    public int getId()
    {
        return id;
    }
    
    public void setId(int id)
    {
        this.id = id;
    }
    
    public void insert()
    {
        id = 0;
        while(service.find(id) != null)
        {
            id += 1;
        }
        
        meeting.setId(id);
        meeting.setTopic(topic);
        meeting.setDuration(duration);       
        meeting.setStartTime(new java.sql.Date(startTime.getTime()));
        meeting.setMax_pers(max_pers);
        service.save(meeting);
    }
    
    public String toString()
    {
        return this.topic;
    }
    
}
