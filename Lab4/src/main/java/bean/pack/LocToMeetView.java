/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bean.pack;

import entitys.Location;
import entitys.Meet_to_loc;
import entitys.Meeting;
import entitys.Pers_to_meet;
import entitys.Person;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.enterprise.context.RequestScoped;
import javax.inject.Named;
import services.LocationService;
import services.Meet_to_locService;
import services.MeetingService;

/**
 *
 * @author manol
 */
@Named("locToMeetView")
@RequestScoped
public class LocToMeetView {
 
    private List<MeetingBean> meetings;
    private List<LocationBean> locations;
    
    private List<Integer> selectedMeetings;
    private List<Integer> selectedLocations;
    
        
    @EJB
    private MeetingService meetingService;
    @EJB
    private LocationService locationService;
    @EJB
    private Meet_to_locService meet_to_locService;
    
    @PostConstruct
    public void init()
    {
        locations = new ArrayList<>();
        meetings = new ArrayList<>();
        
        Connection con = null;
        PreparedStatement pstmt;
        
        System.out.println("init");
        
        for (Location location_entity : locationService.list())
        {
            LocationBean l = new LocationBean();
            l.setName(location_entity.getName());
            l.setId(location_entity.getId());
            locations.add(l);
        }
            
        for (Meeting meeting_entity : meetingService.list())
        {
            MeetingBean m = new MeetingBean();
            m.setTopic(meeting_entity.getTopic());
            m.setId(meeting_entity.getId());
            m.setStartTime( new java.util.Date(meeting_entity.getStartTime().getTime()));
            m.setDuration(meeting_entity.getDuration());
            meetings.add(m);
        }
    }
    
    public List<LocationBean> getLocations() {
        return locations;
    }
    
    public List<MeetingBean> getMeetings() {
        return meetings;
    }
    public List<Integer> getSelectedLocations() {
        return selectedLocations;
    }
    
    public List<Integer> getSelectedMeetings() {
        return selectedMeetings;
    }
        
    public void setSelectedLocations(List<Integer> selectedLocations) {
        this.selectedLocations = selectedLocations;
    }
    
    public void setSelectedMeetings(List<Integer> selectedMeetings) {
        this.selectedMeetings = selectedMeetings;
    }
    
    
    public void attach()
    {
        Location l_selected = locationService.find(selectedLocations.get(0));
        if (l_selected != null){
            for(Integer meetId : this.selectedMeetings)
            {
               Meeting m_selected = meetingService.find(meetId);
               if (m_selected != null)
               {
                   int id = 0;
                   while(meet_to_locService.find(id) != null)
                       ++id;
                   Meet_to_loc mtl = new Meet_to_loc();
                   mtl.setId(id);
                   mtl.setMeeting(m_selected);
                   mtl.setLocation(l_selected);
                   meet_to_locService.save(mtl);
               }
            }
        }
    }
    
}
