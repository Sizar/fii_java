/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bean.pack;

import entitys.Meeting;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.enterprise.context.RequestScoped;
import javax.inject.Named;
import javax.persistence.criteria.CriteriaBuilder;
import services.Meet_to_locService;
import services.MeetingService;
import services.Pers_to_meetService;
import services.PersonService;

/**
 *
 * @author manol
 */

@Named("searchMeetingView")
@RequestScoped
public class SearchMeetingView {
    private String locationName;
    private boolean locationNameChecked;
   
    private String personName;
    private boolean personNameChecked;
    
    private Date startTime;
    private boolean startTimeChecked;
    
    private List<MeetingBean> searchedMeetings;
    
    @EJB
    private MeetingService meetingService;
    
    @PostConstruct
    public void init() 
    {
        searchedMeetings = new ArrayList<>();
        
        for(Meeting m : meetingService.list())
        {
            MeetingBean meeting = new MeetingBean();
            meeting.setId(m.getId());
            meeting.setStartTime( new java.util.Date(m.getStartTime().getTime()));
            meeting.setDuration(m.getDuration());
            meeting.setTopic(m.getTopic());
            searchedMeetings.add(meeting);
        }        
    }
    
    public void setLocationName(String locationName)
    {
        this.locationName = locationName;
    }
    
    public String getLocationName()
    {
        return this.locationName;
    }
        
    public void setPersonName(String personName)
    {
        this.personName = personName;
    }
    
    public String getPersonName()
    {
        return this.personName;
    }
    
    public void setStartTime(Date startTime)
    {
        if(startTime != null)
            this.startTime = new Date(startTime.getTime());
    }
    
    public Date getStartTime()
    {
        return startTime;
    }
    
    public void setLocationNameChecked(boolean locationNameChecked)
    {
        this.locationNameChecked = locationNameChecked;
    }
    
     public boolean getLocationNameChecked()
    {
        return this.locationNameChecked;
    }
     
    public void setPersonNameChecked(boolean personNameChecked)
    {
        this.personNameChecked = personNameChecked;
    }
    
    public boolean getPersonNameChecked()
    {
        return this.personNameChecked;
    }
     
    public void setStartTimeChecked(boolean startTimeChecked)
    {
        this.startTimeChecked = startTimeChecked;
    }
    
    public boolean getStartTimeChecked()
    {
        return this.startTimeChecked;
    }
    
    public List<MeetingBean> getSearchedMeetings() {
        return searchedMeetings;
    }
      
    public void search()
    {
        searchedMeetings.clear();
        for(MeetingBean m : meetingService.search(
                locationName,
                locationNameChecked, 
                personName, 
                personNameChecked, 
                startTime, 
                startTimeChecked))
        {
            searchedMeetings.add(m);
        }      
    }
}
