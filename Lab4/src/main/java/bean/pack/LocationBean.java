/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bean.pack;

import entitys.Location;
import entitys.Person;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.enterprise.context.RequestScoped;
import javax.inject.Named;
import javax.sql.DataSource;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import services.LocationService;
import services.PersonService;

/**
 *
 * @author manol
 */
@Named("locationBean")
@RequestScoped
public class LocationBean {
    
    private Location location;
    private List<Location> locations;

    @EJB
    private LocationService service;
    
    @PostConstruct
    public void init() {
        location = new Location();
        locations = service.list();
    }
    
    
    @Size(max = 100)
    @NotNull
    private String name;
    
    private int id;
    
    static public int idCount = 1;
    static public int UpperBound = 1000000;
    
    public String getName()
    {
        return this.name;
    }
    
    public void setName(String name)
    {
        this.name = name;
    }
    
    public int getId()
    {
        return id;
    }
        
    public void setId(int id)
    {
        this.id = id;
    }
    
    public void insert()
    {
        if(!service.find(name))
        {
            id = 0;
            while(service.find(id) != null)
            {
                id += 1;
            }
       
            location.setName(name);
            location.setId(id);       
            service.save(location);
        }
    }
    
    public String toString()
    {
        return this.name;
    }
     
    public boolean validate()
    {
        return true;
    }
}
