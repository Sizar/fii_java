/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bean.pack;

import entitys.Person;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.enterprise.context.RequestScoped;
import javax.inject.Named;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.sql.DataSource;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import services.PersonService;

/**
 *
 * @author manol
 */
@Named("personBean")
@RequestScoped
public class PersonBean {

    private Person person;
    private List<Person> persons;

    @EJB
    private PersonService service;
    
    @PostConstruct
    public void init() {
        person = new Person();
        persons = service.list();
    }
    
    @Size(max = 100)
    @NotNull
    private String name;
    
    private int id;
    
    public String getName()
    {
        return this.name;
    }
    
    public void setName(String name)
    {
        this.name = name;
    }
    
    public int getId()
    {
        return id;
    }
        
    public void setId(int id)
    {
        this.id = id;
    }
    
    public void insert()
    {
       if(!service.find(name))
       {
            id = 0;
            while(service.find(id) != null)
            {
                id += 1;
            }
       
            person.setName(name);
            person.setId(id);       
            service.save(person);
       }
    }
    
    public String toString()
    {
        return this.name;
    }
     
     public boolean validate()
     {
         return true;
     }
}
