/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bean.pack;

import entitys.Meeting;
import entitys.Pers_to_meet;
import entitys.Person;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.sql.DataSource;
import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.enterprise.context.RequestScoped;
import javax.inject.Named;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import services.AssignService;
import services.MeetingService;
import services.Pers_to_meetService;
import services.PersonService;
import services.TimerSessionBean;

/**
 *
 * @author manol
 */
@Named("basicView")
@RequestScoped
public class BasicView {

    @EJB
    private AssignService assignService;
    
    private List<PersonBean> persons;
    private List<MeetingBean> meetings;
    
    private List<Integer> selectedPersons;
    private List<Integer> selectedMeetings;
    
    private List<String> joinTables;
    
    @EJB
    private MeetingService meetingService;
    @EJB
    private PersonService personService;
    @EJB
    private Pers_to_meetService pers_to_meetService;
    @EJB
    private TimerSessionBean timerSessionBean;
    
    @PostConstruct
    public void init() 
    {    
        timerSessionBean.createTimer(1000);
        persons = new ArrayList<>();
        meetings = new ArrayList<>();
        selectedPersons = new ArrayList<>();
        selectedMeetings = new ArrayList<>();
        
        for(Person p : personService.list())
        {
            PersonBean pb = new PersonBean();
            pb.setName(p.getName());
            pb.setId(p.getId());
            persons.add(pb);
        }
            
        for(Meeting m : meetingService.list())
        {
            MeetingBean meeting = new MeetingBean();
            meeting.setId(m.getId());
            meeting.setStartTime( new java.util.Date(m.getStartTime().getTime()));
            meeting.setDuration(m.getDuration());
            meeting.setTopic(m.getTopic());
            meeting.setMax_pers(m.getMax_pers());
            meetings.add(meeting);
        }            
    }
    
    public List<PersonBean> getPersons() {
        return persons;
    }
    
    public List<MeetingBean> getMeetings() {
        return meetings;
    }
    
    public List<Integer> getSelectedPersons() {
        return selectedPersons;
    }
    
    public List<Integer> getSelectedMeetings() {
        return selectedMeetings;
    }
        
    public void setSelectedPersons(List<Integer> selectedPersons) {
        this.selectedPersons = selectedPersons;
    }
    
    public void setSelectedMeetings(List<Integer> selectedMeetings) {
        this.selectedMeetings = selectedMeetings;
    }
    
    // add meeting to person
    public void attach() throws Exception
    {
        Integer meetId = selectedMeetings.get(0);
        Meeting m_selected = meetingService.find(meetId);
        System.out.println("meeting id: "+ m_selected.getId() +" expected: "+ meetId);
        if ( m_selected != null){
            for(Integer persId : this.selectedPersons)
            {
               Person p_selected = personService.find(persId);
               if (p_selected != null)
               {
                   int id = 0;
                   while(pers_to_meetService.find(id) != null || assignService.containsId(id))
                       ++id;
                   Pers_to_meet ptm = new Pers_to_meet();
                   ptm.setId(id);
                   ptm.setMeeting(m_selected);
                   ptm.setPerson(p_selected);
                   assignService.addItem(ptm);
               }
            }
        }
        assignService.save();
    }
}
