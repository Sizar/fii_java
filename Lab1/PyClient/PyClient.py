import requests
import matplotlib.pyplot as plt
import sys
from datetime import datetime
from concurrent.futures import ThreadPoolExecutor
from time import sleep, time

# this method will return requestTime
def threadWork(letters):
	requestTime = time() * 1000.0
	r = requests.get('http://localhost:8080/Lab1/MyServlet?letters='+letters)
	#print("response:" + r.text)
	responseTime = time() * 1000.0
	return (responseTime-requestTime)


def spamServer(letters, nrThreads):
	listOfFeatures = []
	executor = ThreadPoolExecutor(nrThreads)
	for i in range(nrThreads):
		listOfFeatures.append(executor.submit(threadWork, (letters)))

	allThreadFinised = False
	while not allThreadFinised:
		sleep(1)
		allThreadFinised = True
		for f in listOfFeatures:
			if not f.done():
				allThreadFinised = False
				break

	requestsTimes = 0
	for f in listOfFeatures:
		requestsTimes+=f.result()
	
	print("#################### Req Times infos #########################")
	print("Nr of threads:     " + str(nrThreads))
	print("Avg response time: " + str(requestsTimes/nrThreads) + "ms")
	print("Requests time:     " + str(requestsTimes) + "ms")
	print("##############################################################")
	return requestsTimes/nrThreads


if __name__ == "__main__":
	letters = ""
	if len(sys.argv) != 2:
		letters = input("letter to pass to Servlet: ")
	else:
		letters = sys.argv[1]
	nrSpamingThreads = 1
	nrThreadsList=[]
	avgRequestsTimesList=[]
	while nrSpamingThreads <= 1000:
		avgRequestsTimesList.append(spamServer(letters, nrSpamingThreads))
		nrThreadsList.append(nrSpamingThreads)
		nrSpamingThreads += 50
	

	plt.xlabel("Number of threads")
	plt.ylabel('Average request times')
	plt.plot(nrThreadsList, avgRequestsTimesList)
	plt.show()