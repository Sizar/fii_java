/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package LetterTwister;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

import com.mycompany.lab1.Dictionary.LetterTwister;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author manol
 */
public class LetterTwisterTest {
    
    public LetterTwisterTest() {
    }

    @org.junit.BeforeClass
    public static void setUpClass() throws Exception {
    }

    @org.junit.AfterClass
    public static void tearDownClass() throws Exception {
    }

    @org.junit.Before
    public void setUp() throws Exception { 
    }

    @org.junit.After
    public void tearDown() throws Exception {
    }

    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
    // @Test
    // public void hello() {}
    
    
    @Test
    public void TestLetterTwisterNullWord() 
    {
        LetterTwister twister = new LetterTwister();
        List<String> expectedList = new ArrayList<>();
        
        List<String> list = twister.twistWordLetters(null);
        
        assertNotEquals(list, null);
        assertEquals(0, list.size());
    }
    
     @Test
    public void TestLetterTwisterNormalWordListSize() 
    {
        LetterTwister twister = new LetterTwister();
        List<String> expectedList = new ArrayList<>();
        String word = "word";
        List<String> list = twister.twistWordLetters(word);
        
        assertNotEquals(list, null);
        assertNotEquals(0, list.size());
    }
}
