/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.lab1.Dictionary;

import com.sun.org.apache.xerces.internal.impl.xs.util.StringListImpl;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;


/**
 *
 * @author manol
 */
public class LetterTwister {
    
    private final List<String> res = new LinkedList<>();
    public LetterTwister(){}
    
    public synchronized  List<String> twistWordLetters(final String word)
    {
        res.clear();
        if (word != null)
        {
            /* for each sub word possible length*/
            GenerateSubwords(word);
        }
        return new ArrayList<>(new HashSet<>(res));
    }
    
    /* The main function, input a set of unique numbers, and return their full arrangement */
    List<String> GenerateSubwords(final String word) {
        // record the "path"
        // The selection list here is included in nums
        LinkedList<Integer> track = new LinkedList<>();
        backtrack(word, track);
        return res;
    }
    
    // Path: recorded in track
    // Selection list: elements in nums
    // End condition: all elements in nums appear in track
    void backtrack(final String word, LinkedList<Integer> track) {
        // Trigger end condition
        if (track.size() > 0)
        {
            StringBuilder sb = new StringBuilder();
            for (Integer i : track)
            {
                sb.append(word.charAt(i.intValue()));
            }
            res.add(sb.toString());
        }
        for (int i = 0; i < word.length(); i++) {
            // Exclude illegal choices
            if (track.contains(i))
                continue;
            // make decisions
            track.add(i);
            // Enter the next level of decision tree
            backtrack(word, track);
            // Cancel the selection and return to the previous decision tree
            track.removeLast();
        }
    }
    
    
}
