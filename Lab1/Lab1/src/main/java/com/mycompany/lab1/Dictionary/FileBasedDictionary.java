/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.lab1.Dictionary;

/**
 *
 * @author manol
 */

import com.sun.org.apache.xerces.internal.impl.xs.util.StringListImpl;
import com.sun.org.apache.xerces.internal.xs.StringList;
import java.io.File;
import java.io.FileNotFoundException;
import java.lang.String;
import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Scanner;
import java.util.Vector;

public class FileBasedDictionary implements IDictionary{
    
    private String mPathToFile;
    private List<String> mListOfWords;
    
    public FileBasedDictionary(String pathToFile)
    {
        mPathToFile = pathToFile;
        mListOfWords = new ArrayList(); 
        try
        {
            File file = new File(mPathToFile);
            if (file.exists())
            {
                if (file.canRead())
                {
                    Scanner sc = new Scanner(file);
                    while (sc.hasNextLine())
                    {
                        String line = sc.nextLine();
                        mListOfWords.add(line);
                    } 
                }
                else
                {
                    System.out.println(" The file at path: " + mPathToFile + "can't be read!");
                }
            }
            else
            {
                 System.out.println("The file at path: " + mPathToFile + " doesn't exists!");
            }
        }
        catch(NullPointerException e)
        {
            System.out.println("File pathreceived is null!");
            System.out.println(e.toString());
        }
        catch (SecurityException e)
        {
            System.out.println("File can not be accessed: " + e.getMessage());
        }
        catch (FileNotFoundException e)
        {
            System.out.println("File not found exception: " + e.getMessage());
        }
        catch (IllegalStateException e)
        {
            System.out.println("The scaner was closed: " + e.getMessage());
        }
        catch (NoSuchElementException e)
        {
            System.out.println("No line was found in the file: " + e.getMessage());
        }
    }
    
    @Override
    public boolean hasWord(String word) 
    {
        return mListOfWords.contains(word);
    }
    
}
