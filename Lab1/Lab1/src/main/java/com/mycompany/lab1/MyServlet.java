/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.lab1;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/* My imports*/
import com.mycompany.lab1.Dictionary.*;
import java.util.ArrayList;
import java.util.Iterator;
import javafx.print.Collation;

/**
 *
 * @author manol
 */
@WebServlet(name = "MyServlet", urlPatterns = {"/MyServlet"})
public class MyServlet extends HttpServlet {

    private final IDictionary mDictionary;
    private final LetterTwister mLetterTwister;
    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/plain;charset=UTF-8");
        
        /*Getting the parameters values*/
        StringBuilder sbParamValues = new StringBuilder();
        Iterator<String[]> iterator = request.getParameterMap().values().iterator();
        // while loop
        while (iterator.hasNext())
        {
            sbParamValues.append(iterator.next()[0]);
        }
        
        System.out.print("# METHOD USEDL:" + request.getMethod() + 
                ", Addr:" + request.getRemoteAddr() + 
                ", user-agent: " + request.getHeader("User-Agent") + 
                ", language(s): " + request.getHeader("Accept-Language") +
                ", parameters names: " + request.getParameterMap().keySet().toString() +
                ",parameters values: " + sbParamValues.toString());
        
        try (PrintWriter out = response.getWriter()) { 
            /* TODO output your page here. You may use following sample code. */
            if(request.getParameterMap().containsKey("letters"))
            {
                String lettersReceived = request.getParameter("letters");
                StringBuilder sbWords = new StringBuilder();
                for(String subWord : mLetterTwister.twistWordLetters(lettersReceived))
                {    
                    if (mDictionary.hasWord(subWord))
                    {
                        sbWords.append(subWord);
                        sbWords.append(", ");
                    }
                }
                
                out.println(sbWords.toString());
            }
            else
            {
                /* to do error case */
                out.println("No <letters> parameter was found!");
            }
            out.flush();
            out.close();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
    
    public MyServlet()
    {
        super();
        mDictionary = new FileBasedDictionary("D:\\MASTER\\AN I SEM I\\Java\\fii_java\\Lab1\\Lab1\\src\\main\\java\\com\\mycompany\\lab1\\Resources\\FileWords.txt");
        mLetterTwister = new LetterTwister();
    }
}
