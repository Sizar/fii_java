<%-- 
    Document   : error.jsp
    Created on : Oct 21, 2020, 5:13:03 PM
    Author     : manol
--%>

<%@page  contentType="text/html" pageEncoding="UTF-8" isErrorPage="true"%>
<!DOCTYPE html>
<html>
    <head> 
        <title>Show Error Page</title> 
    </head>
    <body>
        <h1>Opps...</h1>
        <p>Sorry, an error occurred.</p>
        <p>Here is the exception message: </p>
        <% out.print("<h1>" + exception.getMessage()+ "</h1>"); %>
        <% request.setAttribute( "ExceptionSet", 1);%>
    </body>
</html>
