/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entitys;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MapsId;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/**
 *
 * @author manol
 */
@Entity
@Table(name="REPARTITIE")
@NamedQuery(name="getRepartitie", query="SELECT r FROM Repartitie r")
public class Repartitie implements Serializable {

    private static final long serialVersionUID = 1L;
    public static final String GENERATOR = "seqRepartitie";
    
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = GENERATOR)
    private Integer id;

    @Column(name = "id_student")
    public Integer student;
        
    @Column(name = "id_optional")
    public Integer optional;
             
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public void setOptional(Integer optional)
    {
        this.optional = optional;
    }
    
    public Integer getOptional()
    {
        return optional;
    }
    
    
    public void setStudent(Integer student)
    {
        this.student = student;
    }
    
    public Integer getStudent()
    {
        return student;
    }
    
    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Repartitie)) {
            return false;
        }
        Repartitie other = (Repartitie) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entitys.Repartitie[ id=" + id + " ]";
    }
    
}
