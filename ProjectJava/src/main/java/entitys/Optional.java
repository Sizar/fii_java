/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entitys;

import java.io.Serializable;
import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinTable;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

/**
 *
 * @author manol
 */
@Entity
@Table(name="OPTIONAL")
@NamedQuery(name="getOptionals", query="SELECT o FROM Optional o")
public class Optional implements Serializable {
    private static final long serialVersionUID = 1L;
    
    public static final String GENERATOR = "seqOptional";
    
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = GENERATOR)
    private Integer id;

    @Column(name = "denumire")
    private String denumire;
    
    @Column(name = "nrlocuri")
    private Integer nrLocuri;
    
    @Column(name = "nroptiune")
    private Integer nrOptiune;
    
    @Column(name = "an")
    private Integer an;
    
   
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Optional)) {
            return false;
        }
        Optional other = (Optional) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entitys.Optional[ id=" + id + " ]";
    }
    
    
    public void setAn(Integer an)
    {
        this.an=an;
    }
    
    public Integer getAn(){
        return an;
    }
    
    public void setDenumire(@NotNull String denumire)
    {
        this.denumire=denumire;
    }
    
    public String getDenumire(){
        return denumire;
    }
    
    public void setNrLocuri(Integer nrLocuri)
    {
        this.nrLocuri = nrLocuri;
    }
    
    public Integer getNrLocuri(){
        return nrLocuri;
    }
    
      public void setNrOptiune(Integer nrOptiune)
    {
        this.nrOptiune = nrOptiune;
    }
    
    public Integer getNrOptiune(){
        return nrOptiune;
    }
    
}
