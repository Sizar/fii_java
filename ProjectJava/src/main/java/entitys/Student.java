/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entitys;

import java.io.Serializable;
import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinTable;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 *
 * @author manol
 */
@Entity
@Table(name = "STUDENT")
@NamedQuery(name="getStudents", query="SELECT s FROM Student s")
public class Student implements Serializable {

    private static final long serialVersionUID = 1L;
    public static final String GENERATOR = "seqStudent";
    
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = GENERATOR)
    private Integer id;
    
    @Column(name = "FIRSTNAME")
    private String firstname;
    
    @Column(name = "LASTNAME")
    private String lastname;
    
    @Column(name = "EMAIL")
    private String email;
    
    @Column(name = "CNP")
    private String cnp;
    
    @Column(name = "NRMATRICOL")
    private String nrmatricol;
    
    @Column(name = "AN")
    private Integer an;
    
    @Column(name = "CREDITE")
    private Integer credite;
    
    
    public String getFirstname()
    {
        return firstname;
    }
    
    public void setFirstname(String firstname)
    {
        this.firstname = firstname;
    }
    
    
    public String getLastname()
    {
        return lastname;
    }
    
    public void setLastname(String lastname)
    {
        this.lastname = lastname;
    }
    
    public String getEmail()
    {
        return email;
    }
    
    public void setEmail(String email)
    {
        this.email = email;
    }
    
    
    public String getCnp()
    {
        return cnp;
    }
    
    public void setCnp(String cnp)
    {
        this.cnp = cnp;
    }
    
    public void setNrmatricol(String nrmatricol)
    {
        this.nrmatricol=nrmatricol;;
    }
    public String getNrmatricol()
    {
        return nrmatricol;
    }
    
    public Integer getAn(){
        return an;
    }
    
    public void setAn(Integer an)
    {
        this.an = an;
    }
    
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }
    
    public Integer getCredite()
    {
        return credite;
    }
    
    public void setCredite(Integer credite)
    {
        this.credite = credite;
    }
    
    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Student)) {
            return false;
        }
        Student other = (Student) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entitys.Student[ id=" + id + " ]";
    }
    
}
