/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package models.user;

import models.user.UserType;

/**
 *
 * @author manol
 */
public class Guest extends AbstractUser{

    public Guest(String userName, String password) {
        super(userName, password, UserType.Guest);
    }
}
