/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package decorators;

import services.UserInteractionInterface;
import java.util.List;
import javax.decorator.Decorator;
import javax.decorator.Delegate;
import javax.enterprise.event.Observes;
import javax.enterprise.inject.Any;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import models.user.User;

/**
 *
 * @author manol
 */
@Decorator
public abstract class UserInteractionDecorator implements UserInteractionInterface{
    @Inject @Delegate @Any UserInteractionInterface userInteraction;
    
            
    @Override
    public boolean login(User user)
    {
        System.out.println("decorators.UserInteractionDecorator.login() AM ajuns aici!");
        boolean retVal = userInteraction.login(user);
        if(retVal)
        {
            String url;
            if(user.getUserType().isAdmin())
            {
                url = "faces/Admin.xhtml";
            }
            else
            {
                url = "faces/Guest.xhtml";
            }
            FacesContext context = FacesContext.getCurrentInstance();

            HttpServletRequest request = (HttpServletRequest) context
                    .getExternalContext().getRequest();
            HttpSession appsession = request.getSession(true);
            appsession.setAttribute("redirectUrl", url);
            appsession.setAttribute("user", user);
        }
        return retVal;
    }
    @Override
    public boolean register(User user)
    {
        return user == null ? false : userInteraction.register(user);
    }
}
