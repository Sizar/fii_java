/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package services;

import entitys.UserEntity;
import interceptors.Logged;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.EJB;
import javax.enterprise.inject.Default;
import javax.inject.Inject;
import models.user.Admin;
import models.user.Guest;
import models.user.User;
import models.user.UserType;

/**
 *
 * @author manol
 */
@Logged 
@Default
public class UserInteractionService implements UserInteractionInterface
{
    /*@Inject 
    private UserMockRepository userRepository;
    */
    //@Inject 
    //private FileMockRepository fileRepository;
    
   /* @EJB
    private FileRepository fileReop;
    
    @EJB
    private UserRepository userRepo;
    */
    @Override
    public boolean login(final User user) {
        System.out.println("services.UserInteractionService.login()!");
        //return userRepo.contains(toUserEntity(user));
        return false;
    }

    @Override
    public boolean register(final User user) {
        boolean retVal = false;
        /*UserEntity entity = toUserEntity(user);
        if (!userRepo.containsName(entity))
        {
            userRepo.persist(entity);
            retVal = true;
        }*/
        return retVal;
    }
    
    private UserEntity toUserEntity(User user) {
        UserEntity ent = new UserEntity();
        ent.setName( user.getUserName());
        ent.setPassword(user.getPassword());
        //ent.setType(user.getUserType());
        return ent;
    }
    
    private User toUser(UserEntity userEntity) {
        User user = userEntity.getType() == UserType.Admin ?
                new Admin(userEntity.getName(), userEntity.getPassword()):
                new Guest(userEntity.getName(), userEntity.getPassword());
         
        return user;
    }
    
}
