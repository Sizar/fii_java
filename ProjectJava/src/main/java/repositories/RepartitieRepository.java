/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package repositories;

import entitys.Repartitie;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author manol
 */
@Stateless
public class RepartitieRepository {
     @PersistenceContext
    private EntityManager em;

    public List<Repartitie> list() {
        return em.createNamedQuery("getRepartitie").getResultList();
    }

    public Repartitie find(Integer id) {
        return em.find(Repartitie.class, id);
    }

    public Integer save(Repartitie person) {
        em.persist(person);
        return person.getId();
    } 
 
    public void update(Repartitie person) {
        em.merge(person);
    }

    public void delete(Repartitie person) {
        em.remove(em.contains(person) ? person : em.merge(person));
    }
}
