/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package repositories;

import entitys.Optional;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author manol
 */
@Stateless
public class OptionalRepository {
    @PersistenceContext
    private EntityManager em;

    public List<Optional> list() {
        return em.createNamedQuery("getOptionals").getResultList();
    }

    public Optional find(Integer id) {
        return em.find(Optional.class, id);
    }

    public Integer save(Optional person) {
        em.persist(person);
        return person.getId();
    }

    public void update(Optional person) {
        em.merge(person);
    }

    public void delete(Optional person) {
        em.remove(em.contains(person) ? person : em.merge(person));
    }
}
