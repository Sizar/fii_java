/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package repositories;

import entitys.Student;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author manol
 */
@Stateless
public class StudentRepository {
     @PersistenceContext
    private EntityManager em;

    public List<Student> list() {
        return em.createNamedQuery("getStudents").getResultList();
    }

    public Student find(Integer id) {
        return em.find(Student.class, id);
    }

    public Integer save(Student person) {
        em.persist(person);
        return person.getId();
    }

    public void update(Student person) {
        em.merge(person);
    }

    public void delete(Student person) {
        em.remove(em.contains(person) ? person : em.merge(person));
    }
}
