/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package beans;

import javax.enterprise.context.RequestScoped;
import javax.enterprise.inject.Default;
import javax.inject.Named;

/**
 *
 * @author manol
 */
@Default
@Named
@RequestScoped
public class StudentBean {
    private String firstname;
    private String lastname;
    private String email;
    private String cnp;
    private String nrmatricol;
    private Integer an;
    private Integer credite;
    
    public Integer getCredite()
    {
        return credite;
    }
    
    public void setCredite(Integer credite)
    {
        this.credite = credite;
    }
    
    public String getFirstname()
    {
        return firstname;
    }
    
    public void setFirstname(String firstname)
    {
        this.firstname = firstname;
    }
    
    
    public String getLastname()
    {
        return lastname;
    }
    
    public void setLastname(String lastname)
    {
        this.lastname = lastname;
    }
    
    public String getEmail()
    {
        return email;
    }
    
    public void setEmail(String email)
    {
        this.email = email;
    }
    
    
    public String getCnp()
    {
        return cnp;
    }
    
    public void setCnp(String cnp)
    {
        this.cnp = cnp;
    }
    
    public void setNrmatricol(String nrmatricol)
    {
        this.nrmatricol=nrmatricol;;
    }
    public String getNrmatricol()
    {
        return nrmatricol;
    }
    
    public Integer getAn(){
        return an;
    }
    
    public void setAn(Integer an)
    {
        this.an = an;
    }
}
