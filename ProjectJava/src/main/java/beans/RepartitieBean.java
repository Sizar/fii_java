/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package beans;

import interceptors.Logged;
import javax.enterprise.context.RequestScoped;
import javax.enterprise.inject.Default;
import javax.inject.Named;

/**
 *
 * @author manol
 */
@Logged
@Named
@Default
@RequestScoped
public class RepartitieBean {
    private OptionalBean optional;
    private StudentBean student;
    
    public StudentBean getStudent()
    {
        return student;
    }
    
    public void setStudent(StudentBean student)
    {
        this.student = student;
    }
    
    public OptionalBean getOptional()
    {
        return optional;
    }
    
    public void setOptional(OptionalBean optional)
    {
        this.optional = optional;
    }
    
}
