/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package beans;

import entitys.Optional;
import interceptors.Logged;
import javax.ejb.EJB;
import javax.enterprise.context.RequestScoped;
import javax.enterprise.inject.Default;
import javax.inject.Named;
import repositories.OptionalRepository;

/**
 *
 * @author manol
 */
@Logged
@Named
@Default
@RequestScoped
public class OptionalBean{
    
    private int id;
    private String denumire;
    private Integer nrLocuri;
    private Integer nrOptiune;
    private Integer an;
    
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }    
    
    public void setAn(Integer an)
    {
        this.an=an;
    }
    
    public Integer getAn(){
        return an;
    }
    
    public void setDenumire(String denumire)
    {
        System.out.println("beans.OptionalBean.setDenumire()" + denumire);
        this.denumire=denumire;
    }
    
    public String getDenumire(){
        return denumire;
    }
    
    public void setNrLocuri(Integer nrLocuri)
    {
        this.nrLocuri = nrLocuri;
    }
    
    public Integer getNrLocuri(){
        return nrLocuri;
    }
    
    public void setNrOptiune(Integer nrOptiune)
    {
        this.nrOptiune = nrOptiune;
    }
    
    public Integer getNrOptiune(){
        return nrOptiune;
    }
}
