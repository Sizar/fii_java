/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package views;

import entitys.Student;
import interceptors.Logged;
import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.inject.Named;
import repositories.StudentRepository;
import javax.mail.*;
import javax.mail.internet.*;
import javax.mail.util.ByteArrayDataSource;
/**
 *
 * @author manol
 */
@Logged
@Named
@RequestScoped
public class NotifyView {
    @EJB
    private StudentRepository repository;
 
    @Resource(name = "mail/localsmtp")
    private Session mailSession;
    
    public void notifyAllStudents()
    {
        for(Student s : repository.list())
        {
            String email = s.getEmail();
            Integer id = s.getId();
            
            //TODO verify that the student has to apply to all optionals.
            
            Message simpleMail = new MimeMessage(mailSession);
 
            try {
              simpleMail.setSubject("Hello World from Java EE!");
              simpleMail.setRecipient(Message.RecipientType.TO, new InternetAddress(email));

              MimeMultipart mailContent = new MimeMultipart();

              MimeBodyPart mailMessage = new MimeBodyPart();
              mailMessage.setContent("<p>Please register you optional application bu pressing the next link: </p>"
                      + " <p> http://localhost:8080/ProjectJava/SelectionServlet?id="+id+" <p>", "text/html; charset=utf-8");
              mailContent.addBodyPart(mailMessage);

              
              simpleMail.setContent(mailContent);

              Transport.send(simpleMail);

              System.out.println("Message successfully send to: " + email);
            } catch (MessagingException e) {
            }
        }
    }
}
