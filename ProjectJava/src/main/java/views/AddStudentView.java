/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package views;

import beans.StudentBean;
import entitys.Student;
import interceptors.Logged;
import javax.ejb.EJB;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.inject.Named;
import repositories.StudentRepository;

/**
 *
 * @author manol
 */
@Logged
@Named
@RequestScoped
public class AddStudentView {
    @Inject StudentBean studentBean;
    
    @EJB
    private StudentRepository repository;

    public StudentBean getStudentBean()
    {
        return studentBean;
    }
    
    public void setStudentBean(StudentBean studentBean)
    {
        this.studentBean = studentBean;
    }
    
    
    public void save()
    {
        Student s = new Student();
        s.setAn(studentBean.getAn());
        s.setEmail(studentBean.getEmail());
        s.setCnp(studentBean.getCnp());
        s.setFirstname(studentBean.getFirstname());
        s.setLastname(studentBean.getLastname());
        s.setNrmatricol(studentBean.getNrmatricol());
        s.setCredite(studentBean.getCredite());
        repository.save(s);
    }
}
