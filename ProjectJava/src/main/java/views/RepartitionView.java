/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package views;

import beans.OptionalBean;
import beans.RepartitieBean;
import beans.StudentBean;
import entitys.Optional;
import entitys.Repartitie;
import entitys.Student;
import interceptors.Logged;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.enterprise.context.RequestScoped;
import javax.inject.Named;
import repositories.OptionalRepository;
import repositories.RepartitieRepository;
import repositories.StudentRepository;

/**
 *
 * @author manol
 */

@Logged
@Named
@RequestScoped
public class RepartitionView {
    private List<RepartitieBean> repartitions;
    @EJB
    private RepartitieRepository repartitieRepository;
    @EJB
    private OptionalRepository optionalRepository;
    @EJB
    private StudentRepository studentRepository;
    
    
    @PostConstruct
    public void init()
    {
        repartitions = new ArrayList<RepartitieBean>();
        for(Repartitie r: repartitieRepository.list())
        {
            RepartitieBean rb = new RepartitieBean();
            
            StudentBean sb = new StudentBean();
            Student s = studentRepository.find(r.getStudent());
            sb.setAn(s.getAn());
            sb.setEmail(s.getEmail());
            sb.setCnp(s.getCnp());
            sb.setFirstname(s.getFirstname());
            sb.setLastname(s.getLastname());
            sb.setNrmatricol(s.getNrmatricol());
            sb.setCredite(s.getCredite());
            
            
            OptionalBean ob = new OptionalBean();
            Optional o = optionalRepository.find(r.optional);
            ob.setAn(o.getAn());
            ob.setDenumire(o.getDenumire());
            ob.setNrLocuri(o.getNrLocuri());
            ob.setNrOptiune(o.getNrOptiune());
            
            rb.setOptional(ob);
            rb.setStudent(sb);
            
            repartitions.add(rb);
        }
    }
    
    public List<RepartitieBean> getRepartitions()
    {
        return repartitions;
    }
    
}
