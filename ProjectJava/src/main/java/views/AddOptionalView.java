/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package views;

import beans.OptionalBean;
import entitys.Optional;
import interceptors.Logged;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.inject.Named;
import repositories.OptionalRepository;

/**
 *
 * @author manol
 */
@Logged
@Named
@RequestScoped
public class AddOptionalView {
    @Inject OptionalBean optionalBean;
    
    @EJB
    private OptionalRepository repository;
    
    public OptionalBean getOptionalBean()
    {
        return optionalBean;
    }
    
    public void setOptionalBean(OptionalBean optionalBean)
    {
        this.optionalBean = optionalBean;
    }
     
    public void save()
     {
        Optional o = new Optional();
        o.setAn(optionalBean.getAn());
        o.setDenumire(optionalBean.getDenumire());
        o.setNrLocuri(optionalBean.getNrLocuri());
        o.setNrOptiune(optionalBean.getNrOptiune());
        repository.save(o);
     }
}
